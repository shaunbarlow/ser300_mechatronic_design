#include <Arduino.h>
#include <PID_v1.h>
#include "MotorDriver.h"
// #include "Encoder.h"
#include "qencoder.h"
// #include "Movement.h"

/* PINS */
#define PIN_ENC_R_A 2
#define PIN_ENC_R_B 3
#define PIN_ENC_L_A 18
#define PIN_ENC_L_B 19
#define PIN_DRV_L1 7
#define PIN_DRV_L2 6
#define PIN_DRV_R1 4
#define PIN_DRV_R2 5

/* FUNCTION DECLARATIONS */
void leftEncoderISR();
void rightEncoderISR();
void velocityControlRun();

/* OBJECTS */
Motor motorLeft, motorRight;
QEncoder encoderLeft, encoderRight;
// Movement movement;

/* Globals */
double LSetpoint, LInput, LOutput;
double RSetpoint, RInput, ROutput;
double kp = 0.01, ki = 1, kd = 0;

PID leftPID(&LInput, &LOutput, &LSetpoint, kp, ki, kd, DIRECT);
PID rightPID(&RInput, &ROutput, &RSetpoint, kp, ki, kd, DIRECT);

int32_t lastReading;
void setup() {
  Serial.begin(115200);

  motor_construct(&motorLeft, PIN_DRV_L2, PIN_DRV_L1);
  motor_construct(&motorRight, PIN_DRV_R1, PIN_DRV_R2);

  qencoder_construct(&encoderLeft, PIN_ENC_L_A, PIN_ENC_L_B);
  qencoder_construct(&encoderRight, PIN_ENC_R_A, PIN_ENC_R_B);

  qencoder_init(&encoderLeft, leftEncoderISR);
  qencoder_init(&encoderRight, rightEncoderISR);

  leftPID.SetOutputLimits(-1.0, 1.0);
  leftPID.SetMode(AUTOMATIC);

  rightPID.SetOutputLimits(-1.0, 1.0);
  rightPID.SetMode(AUTOMATIC);

  LSetpoint = 2;
  RSetpoint = 2;
}

void loop() {

  velocityControlRun();

}

void leftEncoderISR() {
  direction_t dir = qencoder_updateDirection(&encoderLeft);
  encoderLeft.tickCount += (int32_t)directionScalar[dir];
}

void rightEncoderISR() {
  direction_t dir = qencoder_updateDirection(&encoderRight);
  encoderRight.tickCount += (int32_t)directionScalar[dir];
}

uint32_t lastVelTime;
int32_t lastPosL, lastPosR;
#define VEL_LOOP_PERIOD_MS 50
void velocityControlRun() {
  if (lastVelTime + VEL_LOOP_PERIOD_MS <= millis()) {
    lastVelTime = millis();
    
    int32_t leftDeltaPos = encoderLeft.tickCount - lastPosL;
    lastPosL = encoderLeft.tickCount;

    LInput = (double)leftDeltaPos / (double)VEL_LOOP_PERIOD_MS;
    Serial.print(LInput);
    Serial.print("\t");
    leftPID.Compute();
    motor_setOutput(&motorLeft, LOutput);

    int32_t rightDeltaPos = encoderRight.tickCount - lastPosR;
    lastPosR = encoderRight.tickCount;

    RInput = (double)rightDeltaPos / (double)VEL_LOOP_PERIOD_MS;
    Serial.println(RInput);
    rightPID.Compute();
    motor_setOutput(&motorRight, ROutput);
  }
}