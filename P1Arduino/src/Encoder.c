/*
 * Encoder.c
 *
 *  Created on: 20 Mar 2020
 *      Author: shaun
 */

#include "Encoder.h"


void encoder_construct(Encoder* self, uint8_t pin) {
    self->tickCount = 0;
    self->pin = pin;
    
    self->init = encoder_init;
    self->getReading = encoder_getReading;
    self->reset = encoder_reset;
    self->ISR = encoder_ISR_NOP;
}

void encoder_init(Encoder* self, void (*ISR)()) {
    self->ISR = ISR;
    pinMode(self->pin, INPUT);
    attachInterrupt(digitalPinToInterrupt(self->pin), ISR, CHANGE);
}

int32_t encoder_getReading(Encoder* self) {
    int32_t ticks;
    ticks = self->tickCount;
    return ticks;
}

void encoder_reset(Encoder* self) {
    self->tickCount = 0;
}

void encoder_ISR_NOP() {
    
}
