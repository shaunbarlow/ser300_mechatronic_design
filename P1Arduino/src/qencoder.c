/*
 * qencoder.c
 *
 *  Created on: 3 Apr 2020
 *      Author: shaun
 */

#include "qencoder.h"

direction_t QEM [16] = {STOPPED, BACKWARD, FORWARD, INVALID,
                        FORWARD, STOPPED, INVALID, BACKWARD,
                        BACKWARD, INVALID, STOPPED, FORWARD,
                        INVALID, FORWARD, BACKWARD, STOPPED}; // Quadrature Encoder Matrix



void qencoder_construct(QEncoder* self, uint8_t pinA, uint8_t pinB) {
    self->tickCount = 0;
    self->pinA = pinA;
    self->pinB = pinB;
    self->prevState = 0;
    self->direction = STOPPED;

    self->init = qencoder_init;
    self->getReading = qencoder_getReading;
    self->reset = qencoder_reset;
    self->ISR = qencoder_ISR_NOP;
    self->getPinStates = qencoder_getPinStates;
    self->updateDirection = qencoder_updateDirection;

    directionScalar[STOPPED] = (int8_t)(0);
    directionScalar[FORWARD] = (int8_t)(1);
    directionScalar[BACKWARD] = (int8_t)(-1);
    directionScalar[INVALID] = (int8_t)(0);
}

void qencoder_init(QEncoder* self, void (*ISR)()) {
    self->ISR = ISR;
    pinMode(self->pinA, INPUT);
    pinMode(self->pinB, INPUT);
    attachInterrupt(digitalPinToInterrupt(self->pinA), ISR, CHANGE);  // RISING, FALLING, CHANGE
    attachInterrupt(digitalPinToInterrupt(self->pinB), ISR, CHANGE);
}

int32_t qencoder_getReading(QEncoder* self) {
    int32_t ticks;
    ticks = self->tickCount;
    return ticks;
}

void qencoder_reset(QEncoder* self) {
    self->tickCount = 0;
}

void qencoder_ISR_NOP() {
}

direction_t qencoder_updateDirection(QEncoder* self) {
    uint8_t newState;
    direction_t direction;

    newState = qencoder_getPinStates(self);
    direction = QEM[self->prevState * 4 + newState];
    self->prevState = newState;
    self->direction = direction;
    return direction;
}

/**
 *  @brief  Get the value of digitalRead(pinA) * 2 + digitalRead(pinB) 
 */
uint8_t qencoder_getPinStates(QEncoder* self) {
    uint8_t pinStates;
    pinStates = (digitalRead(self->pinA) * 2) + digitalRead(self->pinB);
    return pinStates;
}
