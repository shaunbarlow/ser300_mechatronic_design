/*
 * MotorDriver.c
 *
 *  Created on: 20 Mar 2020
 *      Author: shaun
 */

#include "MotorDriver.h"
#include <Arduino.h>

void motor_construct(Motor* pMotor, uint8_t pinA, uint8_t pinB) {
	pMotor->init = motor_init;
	pMotor->setOutput = motor_setOutput;

	pMotor->init(pMotor, pinA, pinB);
}


void motor_init(Motor* pMotor, uint8_t pinA, uint8_t pinB) {
	pMotor->pinA = pinA;
	pMotor->pinB = pinB;

	pinMode(pinA, OUTPUT);
	pinMode(pinB, OUTPUT);
}

void motor_setOutput(Motor* pMotor, float output) {
	float fPWM;
	uint8_t iPWM;

	pMotor->output = output;
	fPWM = 0xFF * fabsf(output);
	iPWM = (uint8_t) fPWM;
	if (output > 0) {
		analogWrite(pMotor->pinA, iPWM);
		analogWrite(pMotor->pinB, 0);
	} else {
		analogWrite(pMotor->pinA, 0);
		analogWrite(pMotor->pinB, iPWM);
	}
}

