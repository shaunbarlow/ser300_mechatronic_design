/*
 * Encoder.h
 *
 *  Created on: 20 Mar 2020
 *      Author: shaun
 */

#ifdef __cplusplus
extern "C" {
#endif

#ifndef ENCODER_H_
#define ENCODER_H_

#include <stdint.h>
#include <Arduino.h>

typedef struct Encoder {
    volatile int32_t tickCount;
    uint8_t pin;

    void (*init)();
    int32_t (*getReading)();
    void (*reset)();
    void (*ISR)();
} Encoder;

void encoder_construct(Encoder* self, uint8_t pin);
void encoder_init(Encoder* self, void (*ISR)());
int32_t encoder_getReading(Encoder* self);
void encoder_reset(Encoder* self);
void encoder_ISR_NOP();

#endif /* ENCODER_H_ */

#ifdef __cplusplus
}
#endif