/*
 * qencoder.h
 *
 * Quadrature encoder driver
 * 
 *  Created on: 3 Apr 2020
 *      Author: shaun
 */

#ifdef __cplusplus
extern "C" {
#endif

#ifndef Q_ENCODER_H
#define Q_ENCODER_H

#include <Arduino.h>
#include <stdint.h>

typedef enum direction_t {
    STOPPED,
    FORWARD,
    BACKWARD,
    INVALID
} direction_t;

int8_t directionScalar[4];

typedef struct QEncoder {
    volatile int32_t tickCount;
    uint8_t pinA, pinB;
    uint8_t prevState;
    direction_t direction;

    void (*init)();
    int32_t (*getReading)();
    void (*reset)();
    void (*ISR)();
    uint8_t (*getPinStates)();
    direction_t (*updateDirection)();
} QEncoder;

void qencoder_construct(QEncoder* self, uint8_t pinA, uint8_t pinB);
void qencoder_init(QEncoder* self, void (*ISR)());
int32_t qencoder_getReading(QEncoder* self);
void qencoder_reset(QEncoder* self);
void qencoder_ISR_NOP();
direction_t qencoder_updateDirection(QEncoder* self);
uint8_t qencoder_getPinStates(QEncoder* self);



#endif /* Q_ENCODER_H */

#ifdef __cplusplus
}
#endif