/*
 * MotorDriver.h
 *
 *  Created on: 20 Mar 2020
 *      Author: shaun
 */

#ifdef __cplusplus
extern "C" {
#endif

#ifndef MOTORDRIVER_H_
#define MOTORDRIVER_H_

#include <stdint.h>
#include <math.h>

typedef struct Motor {
	uint8_t pinA;
	uint8_t pinB;
	float output;

	void (*init)();
	void (*setOutput)();

} Motor;

void motor_construct(Motor* pMotor, uint8_t pinA, uint8_t pinB);
void motor_init(Motor* pMotor, uint8_t pinA, uint8_t pinB);
void motor_setOutput(Motor* pMotor, float output);



#endif /* MOTORDRIVER_H_ */

#ifdef __cplusplus
}
#endif