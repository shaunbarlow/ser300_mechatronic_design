/*
 * Movement.h
 *
 *  Created on: 20 Mar 2020
 *      Author: shaun
 */

#ifdef __cplusplus
extern "C" {
#endif

#ifndef MOVEMENT_H_
#define MOVEMENT_H_

typedef enum movestate {
    STOP,
    FORWARD,
    BACKWARD,
    PIVOT_RIGHT,
    PIVOT_LEFT,
} movestate;

typedef struct Movement {
    movestate state;
} Movement;

void movement_enterState(Movement* self, movestate state);

#endif /* MOVEMENT_H_ */

#ifdef __cplusplus
}
#endif
