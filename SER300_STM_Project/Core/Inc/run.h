/*
 * run.h
 *
 *  Created on: Mar 16, 2020
 *      Author: shaun
 */

#ifndef INC_RUN_H_
#define INC_RUN_H_

void run_init();
void run_systick();

#endif /* INC_RUN_H_ */
