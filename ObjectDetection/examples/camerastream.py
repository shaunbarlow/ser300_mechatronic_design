import time
import picamera
import picamera.array
import cv2

with picamera.PiCamera() as camera:
    # camera.start_preview()
    time.sleep(2)
    with picamera.array.PiRGBArray(camera) as stream:
        camera.capture(stream, format='bgr')
        # At this point the image is available as stream.array
        # image = stream.array[:, :, ::-1]
        imgRGB = stream.array
        img = cv2.cvtColor(imgRGB, cv2.COLOR_RGB2BGR)
        # display the image on screen and wait for a keypress
        cv2.imshow("Image", img)
        cv2.waitKey(1)