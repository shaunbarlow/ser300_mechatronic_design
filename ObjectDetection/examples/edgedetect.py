import math
import time
import picamera
import numpy as np
import cv2


class Frame:
    width = 640
    height = 480
    brightness = 130
    rate = 10


class Colour:
    hue = (0, 255)
    sat = (0, 255)
    val = (0, 255)

    def __init__(self, hue_min_max, sat_min_max, val_min_max):
        self.hue = hue_min_max
        self.sat = sat_min_max
        self.val = val_min_max

    def get_thresholds(self):
        lower = np.array([self.hue[0], self.sat[0], self.val[0]])
        upper = np.array([self.hue[1], self.sat[1], self.val[1]])
        return lower, upper


# ORANGE = Colour(hue_min_max=(0, 179),
#                 sat_min_max=(60, 255),
#                 val_min_max=(114, 255))

# ORANGE = Colour(hue_min_max=(0, 80),
#                 sat_min_max=(60, 255),
#                 val_min_max=(114, 255))

ORANGE = Colour(hue_min_max=(0, 30),
                sat_min_max=(60, 255),
                val_min_max=(114, 255))

def empty(*args):
    pass


def add_track_bars():
    cv2.namedWindow("TrackBars")
    cv2.resizeWindow("TrackBars", 640, 240)
    cv2.createTrackbar("Hue Min", "TrackBars", 0, 179, empty)
    cv2.createTrackbar("Hue Max", "TrackBars", 19, 179, empty)
    cv2.createTrackbar("Sat Min", "TrackBars", 60, 255, empty)
    cv2.createTrackbar("Sat Max", "TrackBars", 255, 255, empty)
    cv2.createTrackbar("Val Min", "TrackBars", 114, 255, empty)
    cv2.createTrackbar("Val Max", "TrackBars", 255, 255, empty)


def get_color_thresholds():
    h_min = cv2.getTrackbarPos("Hue Min", "TrackBars")
    h_max = cv2.getTrackbarPos("Hue Max", "TrackBars")
    s_min = cv2.getTrackbarPos("Sat Min", "TrackBars")
    s_max = cv2.getTrackbarPos("Sat Max", "TrackBars")
    v_min = cv2.getTrackbarPos("Val Min", "TrackBars")
    v_max = cv2.getTrackbarPos("Val Max", "TrackBars")
    # print(h_min, h_max, s_min, s_max, v_min, v_max)
    lower = np.array([h_min, s_min, v_min])
    upper = np.array([h_max, s_max, v_max])
    return lower, upper


def filter_colour(img):
    imgHSV = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    lower, upper = ORANGE.get_thresholds()
    mask = cv2.inRange(imgHSV, lower, upper)
    filtered_img = cv2.bitwise_and(img, img, mask=mask)
    return filtered_img


def pre_processing(img):
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img_blur = cv2.GaussianBlur(img_gray, (5, 5), 5)
    img_canny = cv2.Canny(img_blur, 70, 70)
    # return img_canny
    kernel = np.ones((4, 4))
    img_dilate = cv2.dilate(img_canny, kernel, iterations=2)
    return img_dilate
    # img_thresh = cv2.erode(img_dilate, kernel, iterations=1)
    # return img_thresh


OUTLINE_COLOUR = (255, 0, 0)
BALL_RECT_COLOUR = (0, 255, 0)
RED = (0, 0, 255)
balls = []


class Ball:
    x = y = w = h = 0
    balls = []

    def __init__(self, x, y, w, h):
        self.x = x
        self.y = y
        self.w = w
        self.h = h

    def get_center(self):
        center_x = self.x + (0.5 * self.w)
        center_y = self.y + (0.5 * self.h)
        return center_x, center_y

    @staticmethod
    def get_distance_squared_from_robot(self):
        robot_origin_x, robot_origin_y = Frame.width / 2, Frame.height
        ball_x, ball_y = self.get_center()
        dx, dy = ball_x - robot_origin_x, ball_y - robot_origin_y
        distance = (dx ** 2) + (dy ** 2)
        return distance

    @staticmethod
    def sort_balls_by_distance(balls):
        Ball.balls.sort(key=Ball.get_distance_squared_from_robot)
        print(Ball.balls)
        return Ball.balls

    def draw_frame(self, image, colour):
        cv2.rectangle(image, (self.x, self.y), (self.x + self.w, self.y + self.h), colour, 4)

    def get_coords_wrt_origin(self):
        robot_origin_x, robot_origin_y = Frame.width / 2, Frame.height
        ball_x, ball_y = self.get_center()
        dx, dy = ball_x - robot_origin_x, ball_y - robot_origin_y
        return dx, dy


def get_contours(img, imgOutput):
    contours, hierarchy = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    Ball.balls = []
    for cnt in contours:
        area = cv2.contourArea(cnt)
        print(area)
        if area > 150:
            cv2.drawContours(imgOutput, cnt, -1, OUTLINE_COLOUR, 2)

            peri = cv2.arcLength(cnt, True)
            approx = cv2.approxPolyDP(cnt, 0.02 * peri, True)
            x, y, w, h = cv2.boundingRect(approx)
            ball = Ball(x, y, w, h)
            Ball.balls.append(ball)
            cv2.rectangle(imgOutput, (ball.x, ball.y), (ball.x + ball.w, ball.y + ball.h), BALL_RECT_COLOUR, 4)
    return imgOutput





def main():
    with picamera.PiCamera() as camera:
        camera.resolution = (Frame.width, Frame.height)
        camera.framerate = 10
        # add_track_bars()
        time.sleep(1)
        while True:
            image = np.empty((Frame.width * Frame.height * 3,), dtype=np.uint8)
            camera.capture(image, 'bgr', )
            image = image.reshape((Frame.height, Frame.width, 3))
            flipped = cv2.flip(image, flipCode=-1)
            image_color_filtered = filter_colour(flipped)
            image_processed = pre_processing(image_color_filtered)
            # processed_copy = image_color_filtered.copy()
            # color_processed_copy = cv2.cvtColor(processed_copy, cv2.COLOR_GRAY2BGR)
            image_contours = get_contours(image_processed, image_color_filtered)
            sorted_balls = Ball.sort_balls_by_distance(Ball.balls)
            if sorted_balls:
                closest_ball = sorted_balls[0]
                closest_ball.draw_frame(image_contours, RED)

            cv2.imshow("Image", image_contours)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break


if __name__ == '__main__':
    main()