import time
import picamera
import numpy as np
import cv2


class Frame:
    width = 640
    height = 480
    brightness = 130


with picamera.PiCamera() as camera:
    camera.resolution = (Frame.width, Frame.height)
    camera.framerate = 24
    time.sleep(2)
    while True:
        image = np.empty((Frame.width * Frame.height * 3,), dtype=np.uint8)
        camera.capture(image, 'bgr')
        image = image.reshape((Frame.height, Frame.width, 3))
        cv2.imshow("Image", image)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break