/*
 * comm.h
 *
 *  Created on: May 24, 2020
 *      Author: shaun
 */

#ifndef INC_COMM_H_
#define INC_COMM_H_

#include "main.h"

typedef enum commState_t {
	NOT_READY,
	TX_READY,
	TX_IN_PROGRESS,
	TX_COMPLETE,
} commState_t;

enum commLockStatus {
	LOCK_OK,
	LOCK_ERROR,
};

uint8_t I2C_LockAddress;

uint8_t comm_getLock(uint8_t address);
uint8_t comm_putLock(uint8_t address);
uint8_t comm_checkLock(uint8_t address);


#endif /* INC_COMM_H_ */
