/*
 * pid.h
 *
 *  Created on: Apr 12, 2020
 *      Author: shaun
 */

#ifndef INC_PID_H_
#define INC_PID_H_

#include "main.h"

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))
#define CAP(input, min, max) MIN(MAX(input, min), max)


typedef struct pid_t {
	double kp, ki, kd;
	double integralError;
	double lastError;
	double maxIntegralError;
	double minOutput, maxOutput;
	uint32_t lastTime;
	double setpoint;
	double output;
	uint8_t locked;
} pidControl_t;

void pid_construct(pidControl_t* self, double kp, double ki, double kd,
		double maxOutput, double minOutput,
		double maxIntegralError);
void pid_init(pidControl_t* self);
double pid_run(pidControl_t* self, double input);
void pid_set(pidControl_t* self, double setpoint);

#endif /* INC_PID_H_ */
