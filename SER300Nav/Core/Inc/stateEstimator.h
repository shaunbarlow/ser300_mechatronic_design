/*
 * stateEstimator.h
 *
 *  Created on: Apr 12, 2020
 *      Author: shaun
 */

#ifndef INC_STATEESTIMATOR_H_
#define INC_STATEESTIMATOR_H_

#include "main.h"

typedef struct stateEstimator_t {
	double velocityDemand;
	uint16_t position;
	double velocity;
	double maxAcceleration;
	uint32_t lastTime;
} stateEstimator_t;

void stateEstimator_construct(stateEstimator_t* self);
void stateEstimator_init(stateEstimator_t* self, uint16_t position);
void stateEstimator_update(stateEstimator_t* self, uint16_t position);
void stateEstimator_setVelocityDemand(stateEstimator_t* self, double velocity);
#endif /* INC_STATEESTIMATOR_H_ */
