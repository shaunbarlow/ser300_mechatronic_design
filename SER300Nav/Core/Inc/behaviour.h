/*
 * behaviour.h
 *
 *  Created on: 25 May 2020
 *      Author: shaun
 */

#ifndef INC_BEHAVIOUR_H_
#define INC_BEHAVIOUR_H_

#include "stateEstimator.h"

enum ballFound {
	NO_BALL,
	BALL_FOUND
};


uint8_t behaviour_driveToBall(stateEstimator_t* pStateEstLeft,
		stateEstimator_t* pStateEstRight, uint16_t upperDist, uint16_t lowerDist);
uint8_t behaviour_pointToBall(stateEstimator_t* pStateEstLeft,
		stateEstimator_t* pStateEstRight, uint16_t upperDist, uint16_t lowerDist);
uint8_t behaviour_checkForBallRight(uint16_t upperDist, uint16_t lowerDist);
uint8_t behaviour_checkForBallDelta(uint16_t newDist, uint16_t oldDist);

#endif /* INC_BEHAVIOUR_H_ */
