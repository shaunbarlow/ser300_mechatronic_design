/*
 * TOF_VL53L0X.h
 *
 *  Created on: Apr 25, 2020
 *      Author: shaun
 */

#ifndef INC_TOFVL53L0X_H_
#define INC_TOFVL53L0X_H_

#include "stm32f4xx_hal.h"
#include "vl53l0x_api.h"
#include "comm.h"

#define TOF_DEFAULT_ADDRESS 0x52


typedef struct tofVL53L0X_t {
	VL53L0X_Dev_t  vl53l0x_c; // center module
	VL53L0X_DEV    Dev; // = &vl53l0x_c;
	I2C_HandleTypeDef* phi2c;
	GPIO_TypeDef* xshutPort;
	uint16_t xshutPin;
	GPIO_TypeDef* gpio1Port;
	uint16_t gpio1Pin;
	IRQn_Type IRQn;

	uint8_t address;
	uint8_t tofID;

	volatile uint8_t dataReady;
	volatile uint8_t dataWaiting;
	VL53L0X_RangingMeasurementData_t RangingData;

	uint16_t distance;
	uint16_t lowPassDistance;
	float alpha;

	uint8_t rxBuffer[20];
	commState_t commState;
} tofVL53L0X_t;

void tof_construct(tofVL53L0X_t* self, I2C_HandleTypeDef* phi2c, uint8_t address,
		GPIO_TypeDef* xshutPort, uint16_t xshutPin,
		GPIO_TypeDef* gpio1Port, uint16_t gpio1Pin,
		IRQn_Type interruptNumber);
void tof_init(tofVL53L0X_t* self);
uint16_t tof_getDistance(tofVL53L0X_t* self);
uint16_t tof_getLowPassDistance(tofVL53L0X_t* self);
void tof_continuousMeasurementInterrupt(tofVL53L0X_t* self, uint16_t pin);
void tof_DMA_getContinuousMeasurement(tofVL53L0X_t* self);
void tof_receiveMeasurement(tofVL53L0X_t* self);
void tof_DMA_receiveMeasurement(tofVL53L0X_t* self);
uint8_t tof_getList(tofVL53L0X_t* tofList);
void tof_setCommState(tofVL53L0X_t* self, commState_t state);
void tof_onExti(tofVL53L0X_t* self, uint16_t pin);
void tof_onRxCplt(tofVL53L0X_t* self, uint8_t address);
void tof_commCheck(tofVL53L0X_t* self);

#endif /* INC_TOFVL53L0X_H_ */
