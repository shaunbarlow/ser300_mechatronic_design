/*
 * GY87.h
 *
 *  Created on: Apr 9, 2020
 *      Author: shaun
 */

#ifndef INC_GY87_H_
#define INC_GY87_H_

#include "main.h"
#include "MPU6050.h"
#include "HMC5883L.h"
#include "kalman.h"
#include <math.h>

#define DEG_TO_RAD (M_PI / 180.0)
#define RAD_TO_DEG (180.0 / M_PI)

typedef struct GY87_t {
	MPU6050_t imu;
	HMC5883L_t mag;
	kalman_t kalmanX, kalmanY, kalmanZ;

	double roll, pitch, yaw;
	double kalAngleX, kalAngleY, kalAngleZ;
	uint32_t lastUpdateTime;
} GY87_t;

#define HMC5883L_ADDR 0x1E << 1

void GY87_construct(GY87_t* self, I2C_HandleTypeDef *phi2c);
void GY87_init(GY87_t* self);
void GY87_update(GY87_t* self);
void GY87_estimateRollPitch(GY87_t* self, double dt);
void GY87_estimateYaw(GY87_t* self, double dt);
void GY87_updatePitchRoll(GY87_t* self);
void GY87_updateYaw(GY87_t* self);
void GY87_error();

#endif /* INC_GY87_H_ */
