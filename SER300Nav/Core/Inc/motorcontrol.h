/*
 * motorcontrol.h
 *
 *  Created on: Apr 13, 2020
 *      Author: shaun
 */

#ifndef INC_MOTORCONTROL_H_
#define INC_MOTORCONTROL_H_

#include "main.h"
#include "stateEstimator.h"
#include "pid.h"
#include "motordriver.h"
#include "encoder.h"

typedef struct motorControl_t {
	stateEstimator_t* stateEstimator;
	pidControl_t* velocityPID;
	encoder_t* encoder;
	motor_t* motor;
} motorControl_t;



void motorControl_construct(motorControl_t* self,
		stateEstimator_t* stateEstimator,
		pidControl_t* velocityPID,
		encoder_t* encoder,
		motor_t* motor);
void motorControl_init(motorControl_t* self);
void motorControl_run(motorControl_t* self);

#endif /* INC_MOTORCONTROL_H_ */
