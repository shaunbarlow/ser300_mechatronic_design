/*
 * HMC5883L.h
 *
 *  Created on: 10 Apr 2020
 *      Author: shaun
 */

#ifndef INC_HMC5883L_H_
#define INC_HMC5883L_H_

#include "main.h"
#include "comm.h"

#define HMC5883L_ADDR 0x1E << 1

/* Registers */
#define CONFIG_A			0x00 // Configuration Register A
#define CONFIG_B			0x01 // Configuration Register B
#define MODE				0x02 // Mode Register
#define	DATA_OUTPUT_X_MSB	0x03 // Data Output X MSB Register
#define DATA_OUTPUT_X_LSB	0x04 // Data Output X LSB Register
#define	DATA_OUTPUT_Z_MSB	0x05 // Data Output Z MSB Register
#define DATA_OUTPUT_Z_LSB	0x06 // Data Output Z LSB Register
#define	DATA_OUTPUT_Y_MSB	0x07 // Data Output Y MSB Register
#define DATA_OUTPUT_Y_LSB	0x08 // Data Output Y LSB Register
#define STATUS				0x09 // Status Register
#define IDENTIFICATION_A	0x0A // Identification Register A
#define	IDENTIFICATION_B	0x0B // Identification Register B
#define	IDENTIFICATION_C	0x0C // Identification Register C

/* Register options */
#define MODE_CONTINUOUS		0x00 // Continuous-Measurement Mode
#define MODE_SINGLE			0x01 // Single-Measurement Mode (Default)

#define CONFIG_A_8AVERAGE_15HZ_NOBIAS	0x70 // 8-average, 15 Hz default, normal measurement
#define CONFIG_A_1AVERAGE_15HZ_POSBIAS	0x11
#define CONFIG_A_1AVERAGE				0x00
#define CONFIG_A_8AVERAGE 				0x60
#define CONFIG_A_15HZ					0x10
#define CONFIG_A_BIAS_POS				0x01
#define CONFIG_A_BIAS_NEG				0x02
#define CONFIG_A_BIAS_NONE				0x00

typedef struct HMC5883L_t {
	I2C_HandleTypeDef* phi2c;
	uint8_t address;
	uint8_t timeout;
	int16_t x, y, z;
	double gains[3]; // xyz order
	double offsets[3];
	double yaw;
	uint8_t rxBuffer[20];
	commState_t commState;
	uint32_t readTime;
	uint32_t period;
} HMC5883L_t;

void HMC5883L_construct(HMC5883L_t* self, I2C_HandleTypeDef* phi2c);
void HMC5883L_init(HMC5883L_t* self);
void HMC5883L_read(HMC5883L_t* self);
void HMC5883L_readDMA(HMC5883L_t* self);
void HMC5883L_processMeasurement(HMC5883L_t* self, uint8_t* rawReading);
void HMC5883L_calibrate(HMC5883L_t* self);
void HMC5883L_error(HMC5883L_t* self);
double HMC5883L_getYaw(HMC5883L_t* self);
void HMC5883L_setCommState(HMC5883L_t* self, commState_t state);
void HMC5883L_onRxCplt(HMC5883L_t* self, uint8_t address);
uint8_t HMC5883L_commCheck(HMC5883L_t* self);

#endif /* INC_HMC5883L_H_ */
