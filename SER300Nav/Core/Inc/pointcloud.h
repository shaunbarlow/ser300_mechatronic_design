/*
 * pointcloud.h
 *
 *  Created on: May 25, 2020
 *      Author: shaun
 */

#ifndef INC_POINTCLOUD_H_
#define INC_POINTCLOUD_H_

#include "main.h"

#define NUM_POINTS 360

typedef struct pointCloud_t {
	uint16_t points[NUM_POINTS];
	int32_t edges[NUM_POINTS];
	uint16_t numEdges;
} pointCloud_t;

void pointCloud_clear(pointCloud_t* self);
void pointCloud_setPoint(pointCloud_t* self, uint16_t angle, uint16_t value);
uint16_t pointCloud_getPoint(pointCloud_t* self, uint16_t angle);
uint16_t pointCloud_getEdges(pointCloud_t* self, int32_t* pEdgeArray, uint16_t threshold);
uint16_t pointCloud_populateEdges(pointCloud_t* self, uint16_t threshold);
uint8_t pointCloud_getDegreesForObjectOfWidth(uint16_t distance, uint16_t width);
//uint16_t pointCloud_getNextPositiveEdge(pointCloud_t* self, int32_t* pEdges, uint16_t numEdges);
//uint16_t pointCloud_getNextNegativeEdge(pointCloud_t* self, int32_t* pEdges, uint16_t numEdges);
uint16_t pointCloud_findNextObstacleOfWidth(pointCloud_t* self, uint16_t width);

#endif /* INC_POINTCLOUD_H_ */
