/*
 * kalman.h
 *
 *  Created on: 11 Apr 2020
 *      Author: shaun
 *
 *  Adapted from C++ implementation by:
 *   Kristian Lauszus, TKJ Electronics
 *	 Web      :  http://www.tkjelectronics.com
 *	 e-mail   :  kristianl@tkjelectronics.com
 */

#ifndef INC_KALMAN_H_
#define INC_KALMAN_H_

typedef struct kalman_t {
	double Q_angle; // Process noise variance for the accelerometer
	double Q_bias; // Process noise variance for the gyro bias
	double R_measure; // Measurement noise variance - this is actually the variance of the measurement noise

	double angle; // The angle calculated by the Kalman filter - part of the 2x1 state vector
	double bias; // The gyro bias calculated by the Kalman filter - part of the 2x1 state vector
	double rate; // Unbiased rate calculated from the rate and the calculated bias - you have to call getAngle to update the rate

	double P[2][2]; // Error covariance matrix - This is a 2x2 matrix
	double K[2]; // Kalman gain - This is a 2x1 vector
	double y; // Angle difference
	double S; // Estimate error
} kalman_t;

void kalman_construct(kalman_t* self);
double kalman_getAngle(kalman_t* self, double newAngle, double newRate, double dt);
void kalman_setAngle(kalman_t* self, double newAngle); // Used to set angle, this should be set as the starting angle
double kalman_getRate(kalman_t* self); // Return the unbiased rate

/* These are used to tune the Kalman filter */
void kalman_setQangle(kalman_t* self, double newQ_angle);
void kalman_setQbias(kalman_t* self, double newQ_bias);
void kalman_setRmeasure(kalman_t* self, double newR_measure);

double kalman_getQangle(kalman_t* self);
double kalman_getQbias(kalman_t* self);
double kalman_getRmeasure(kalman_t* self);

#endif /* INC_KALMAN_H_ */
