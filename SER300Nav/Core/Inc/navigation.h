/*
 * navigation.h
 *
 *  Created on: Apr 13, 2020
 *      Author: shaun
 */

#ifndef SRC_NAVIGATION_H_
#define SRC_NAVIGATION_H_

#include "main.h"
#include "pid.h"
#include "GY87.h"

typedef struct navigation_t {
	double currentHeading;
	double targetHeading;
	pidControl_t headingPID;
	double leftVelocityOutput;
	double rightVelocityOutput;
	GY87_t* imu;
} navigation_t;

void nav_construct(navigation_t* self, GY87_t* imu,
		double kp, double ki, double kd,
		double minOutput, double maxOutput);
double nav_steerToHeading(navigation_t* self, double target);
double nav_getSmallestAngleBetween(double target, double origin);
double wrapAngle(double angle);
uint8_t testAngles();

#endif /* SRC_NAVIGATION_H_ */
