/*
 * motordriver.h
 *
 *  Created on: 11 Apr 2020
 *      Author: shaun
 */

#ifndef INC_MOTORDRIVER_H_
#define INC_MOTORDRIVER_H_

#include "main.h"
#include <math.h>

typedef struct motor_t {
	TIM_HandleTypeDef* phtim;
	uint32_t channelA;
	uint32_t channelB;
	float output;
} motor_t;

void motor_construct(motor_t* self, TIM_HandleTypeDef* phtim,
		uint32_t channelA, uint32_t channelB);
void motor_init(motor_t* self);
void motor_setOutput(motor_t* self, double output);

#endif /* INC_MOTORDRIVER_H_ */
