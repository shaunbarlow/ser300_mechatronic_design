/*
 * MPU6050.h
 *
 *  Created on: Apr 10, 2020
 *      Author: shaun
 */

#ifndef INC_MPU6050_H_
#define INC_MPU6050_H_

#include "main.h"
#include "comm.h"


#define MPU6050_ADDR 0x68 << 1

/* MPU6050 Registers */
#define SMPRT_DIV 				0x19
#define CONFIG 					0x1A
#define GYRO_CONFIG 			0x1B
#define ACCEL_CONFIG			0x1C
#define INT_PIN_CFG 			0x37
#define ACCEL_XOUT_H			0x3B
#define ACCEL_XOUT_L			0x3C
#define ACCEL_YOUT_H			0x3D
#define ACCEL_YOUT_L			0x3E
#define ACCEL_ZOUT_H			0x3F
#define ACCEL_ZOUT_L			0x40
#define TEMP_OUT_H 				0x41
#define TEMP_OUT_L				0x42
#define GYRO_XOUT_H				0x43
#define GYRO_XOUT_L				0x44
#define GYRO_YOUT_H				0x45
#define GYRO_YOUT_L				0x46
#define GYRO_ZOUT_H				0x47
#define GYRO_ZOUT_L				0x48
#define PWR_MGMT_1				0x6B
#define WHO_AM_I				0x75


#define SMPRT_1000HZ 				7 // Set the sample rate to 1000Hz - 8kHz/(7+1) = 1000Hz
#define CONFIG_DISABLE_ALL 			0 // Disable FSYNC and set 260 Hz Acc filtering, 256 Hz Gyro filtering, 8 KHz sampling
#define GYRO_CONFIG_FULL_SCALE_250 	0 // Set Gyro Full Scale Range to ±250deg/s
#define ACCEL_CONFIG_FULL_SCALE_2	0 // Set Accelerometer Full Scale Range to ±2g
#define INT_PIN_CFG_I2C_BYPASS_EN	2 // Allow bypass
#define PWR_MGMT_1_PLL_X_AXIS_GYRO_AND_SLEEP_DISABLE	1 // PLL with X axis gyroscope reference and disable sleep mode


typedef struct MPU6050_t {
	I2C_HandleTypeDef* phi2c;
	uint8_t address;
	int16_t accX, accY, accZ;
	int16_t tempRaw;
	int16_t gyroX, gyroY, gyroZ;
	uint8_t rxBuffer[20];
	commState_t commState;
	uint32_t readTime;
	uint32_t period;
} MPU6050_t;

void MPU6050_construct(MPU6050_t* self, I2C_HandleTypeDef* phi2c1);
void MPU6050_init(MPU6050_t* self);
void MPU6050_read(MPU6050_t* self);
void MPU6050_error(MPU6050_t* self);
void MPU6050_readDMA(MPU6050_t* self);
void MPU6050_onRxCplt(MPU6050_t* self, uint8_t address);
uint8_t MPU6050_commCheck(MPU6050_t* self);
void MPU6050_setCommState(MPU6050_t* self, commState_t state);

#endif /* INC_MPU6050_H_ */
