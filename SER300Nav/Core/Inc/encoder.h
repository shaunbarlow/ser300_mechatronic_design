/*
 * encoder.h
 *
 *  Created on: Apr 16, 2020
 *      Author: shaun
 */

#ifndef INC_ENCODER_H_
#define INC_ENCODER_H_

#include "main.h"

#define ENC_MIDDLE 0x7FFF
#define ENC_INIT 0x0FFF
#define ENC_MAX 0xFFFF

typedef struct encoder_t {
	TIM_HandleTypeDef* phtim;
	uint16_t lastReading;
	int32_t position;
	void (*init)();
	uint16_t (*getReading)();
} encoder_t;

void encoder_construct(encoder_t* self, TIM_HandleTypeDef* phtim);
void encoder_init(encoder_t* self);
uint16_t encoder_getReading(encoder_t* self);

#endif /* INC_ENCODER_H_ */
