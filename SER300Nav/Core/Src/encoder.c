/*
 * encoder.c
 *
 *  Created on: Apr 16, 2020
 *      Author: shaun
 */


#include "encoder.h"

void encoder_construct(encoder_t* self, TIM_HandleTypeDef* phtim) {
	self->phtim = phtim;
	self->init = encoder_init;
	self->getReading = encoder_getReading;
}

void encoder_init(encoder_t* self) {
	HAL_TIM_Encoder_Start(self->phtim, TIM_CHANNEL_ALL);
	__HAL_TIM_SetCounter(self->phtim, ENC_INIT);
}

uint16_t encoder_getReading(encoder_t* self) {
	return __HAL_TIM_GetCounter(self->phtim);
}
