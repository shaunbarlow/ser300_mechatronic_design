/*
 * pid.c
 *
 *  Created on: Apr 12, 2020
 *      Author: shaun
 *
 * Reference: http://robotsforroboticists.com/pid-control/
 */

#include "pid.h"

void pid_construct(pidControl_t* self, double kp, double ki, double kd,
		double maxOutput, double minOutput,
		double maxIntegralError) {
	self->kp = kp;
	self->ki = ki;
	self->kd = kd;
	self->maxIntegralError = maxIntegralError;
	self->minOutput = minOutput;
	self->maxOutput = maxOutput;
}

void pid_init(pidControl_t* self) {
	self->integralError = 0;
	self->lastError = 0;
	self->lastTime = HAL_GetTick();
	self->setpoint = 0;
	self->output = 0;
	self->locked = 0;
}

double pid_run(pidControl_t* self, double input) {
	if (self->locked) return self->output;
	double error, dError, dt;
	uint32_t now;

	now = HAL_GetTick(); // in milliseconds
	dt = (double)(now - self->lastTime) * 0.001; // dt in seconds
	self->lastTime = now;

	/* Proportional */
	error = self->setpoint - input;

	/* Integral */
	self->integralError += error;
	if (self->maxIntegralError > 0) {
		self->integralError = CAP(self->integralError,
				-self->maxIntegralError, self->maxIntegralError);
	}

	/* Derivative */
	dError = error - self->lastError;
	self->lastError = error;

	/* Output */
	if (dt <= 0) {
		self->output = (self->kp * error);
		self->integralError = 0;
	} else {
		self->output = (self->kp * error)
					+ (self->ki * self->integralError * dt)
					+ (self->kd * dError / dt);
	}
	self->output = CAP(self->output, self->minOutput, self->maxOutput);
	return self->output;
}

void pid_set(pidControl_t* self, double setpoint) {
	uint8_t status;
	self->locked = 1;
	if (setpoint > 21474836) {
		status = 1;
	}
	self->setpoint = setpoint;
	self->locked = 0;
}


