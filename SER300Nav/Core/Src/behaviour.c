/*
 * behaviour.c
 *
 *  Created on: 25 May 2020
 *      Author: shaun
 */

#include "behaviour.h"

#define TOF_OFFSET 0
#define MARGIN 100
#define BALL_DIA 40


uint8_t behaviour_driveToBall(stateEstimator_t* pStateEstLeft,
		stateEstimator_t* pStateEstRight, uint16_t upperDist, uint16_t lowerDist) {
	if (behaviour_checkForBallRight(upperDist, lowerDist) == BALL_FOUND) {
		stateEstimator_setVelocityDemand(pStateEstLeft, 200);
		stateEstimator_setVelocityDemand(pStateEstRight, 200);
		return BALL_FOUND;
	} else {
		stateEstimator_setVelocityDemand(pStateEstLeft, 200);
		stateEstimator_setVelocityDemand(pStateEstRight, -200);
		return NO_BALL;
	}
}

uint8_t behaviour_pointToBall(stateEstimator_t* pStateEstLeft,
		stateEstimator_t* pStateEstRight, uint16_t upperDist, uint16_t lowerDist) {
	if (behaviour_checkForBallRight(upperDist, lowerDist) == BALL_FOUND) {
		stateEstimator_setVelocityDemand(pStateEstLeft, 0);
		stateEstimator_setVelocityDemand(pStateEstRight, 0);
		return BALL_FOUND;
	} else {
		stateEstimator_setVelocityDemand(pStateEstLeft, 300);
		stateEstimator_setVelocityDemand(pStateEstRight, -300);
		return NO_BALL;
	}
}

uint8_t behaviour_checkForBallRight(uint16_t upperDist, uint16_t lowerDist) {
	if (upperDist > (lowerDist + TOF_OFFSET + MARGIN)) {
		return BALL_FOUND;
	}
	return NO_BALL;
}

uint8_t behaviour_checkForBallDelta(uint16_t newDist, uint16_t oldDist) {
	if (newDist > oldDist + 40) {
		return BALL_FOUND;
	}
	return NO_BALL;
}


