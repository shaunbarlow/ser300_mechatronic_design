/*
 * motordriver.c
 *
 *  Created on: 11 Apr 2020
 *      Author: shaun
 */

#include "motordriver.h"

void motor_construct(motor_t* self, TIM_HandleTypeDef* phtim,
		uint32_t channelA, uint32_t channelB) {
	self->phtim = phtim;
	self->channelA = channelA;
	self->channelB = channelB;
}

void motor_init(motor_t* self) {
	HAL_TIM_Base_Start_IT(self->phtim);
	HAL_TIM_PWM_Start_IT(self->phtim, self->channelA);
	__HAL_TIM_SetCompare(self->phtim, self->channelA, 1);
	HAL_TIM_PWM_Start_IT(self->phtim, self->channelB);
	__HAL_TIM_SetCompare(self->phtim, self->channelB, 1);
//	motor_setOutput(self, 0);
}

void motor_setOutput(motor_t* self, double output) {
	if (mode == mode_noMotors) {
		output = 0;
	}
	uint32_t pulseA, pulseB;
	self->output = (float)output;
	pulseA = (self->phtim->Init.Period) * (0.5 * (1 - output));
	pulseB = (self->phtim->Init.Period) * (0.5 * (1 + output));
	__HAL_TIM_SetCompare(self->phtim, self->channelA, pulseA);
	__HAL_TIM_SetCompare(self->phtim, self->channelB, pulseB);
}
