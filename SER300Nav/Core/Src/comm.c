/*
 * comm.c
 *
 *  Created on: May 24, 2020
 *      Author: shaun
 */

#include "comm.h"

#define NO_ADDRESS 0


uint8_t comm_getLock(uint8_t address) {
	if (I2C_LockAddress == NO_ADDRESS) {
		I2C_LockAddress = address;
		return LOCK_OK;
	} else {
		return LOCK_ERROR;
	}
}

uint8_t comm_putLock(uint8_t address) {
	if (I2C_LockAddress == address) {
		I2C_LockAddress = NO_ADDRESS;
		return LOCK_OK;
	} else {
		return LOCK_ERROR;
	}
}
