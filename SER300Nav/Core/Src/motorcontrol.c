/*
 * motorcontrol.c
 *
 *  Created on: Apr 13, 2020
 *      Author: shaun
 */

#ifndef SRC_MOTORCONTROL_C_
#define SRC_MOTORCONTROL_C_

#include "motorcontrol.h"

double limitAccel(double old, double new, double maxAccel);

void motorControl_construct(motorControl_t* self,
		stateEstimator_t* stateEstimator,
		pidControl_t* velocityPID,
		encoder_t* encoder,
		motor_t* motor) {
	self->stateEstimator = stateEstimator;
	self->velocityPID = velocityPID;
	self->encoder = encoder;
	self->motor = motor;
}

void motorControl_init(motorControl_t* self) {
	stateEstimator_init(self->stateEstimator, encoder_getReading(self->encoder));
}

void motorControl_run(motorControl_t* self) {
	stateEstimator_update(self->stateEstimator, encoder_getReading(self->encoder));
	self->velocityPID->setpoint = limitAccel(self->velocityPID->setpoint, self->stateEstimator->velocityDemand, self->stateEstimator->maxAcceleration);
	double motorOutput = pid_run(self->velocityPID, self->stateEstimator->velocity);
	motor_setOutput(self->motor, motorOutput);
}

double limitAccel(double old, double new, double maxAccel) {
	double out;
	if (new > old) {
		out = fmin(new, old + maxAccel);
	} else {
		out = fmax(new, old - maxAccel);
	}
	return out;
}

#endif /* SRC_MOTORCONTROL_C_ */
