/*
 * stateEstimator.c
 *
 *  Created on: Apr 12, 2020
 *      Author: shaun
 */

#include "stateEstimator.h"
#include "encoder.h"

void stateEstimator_construct(stateEstimator_t* self) {
	self->lastTime = HAL_GetTick();
	self->velocity = 0;
	self->position = 0;
	self->maxAcceleration = 20.0;
}

void stateEstimator_init(stateEstimator_t* self, uint16_t position) {
	self->position = position;
	self->lastTime = HAL_GetTick(); // in milliseconds
	self->velocity = 0;
}

void stateEstimator_update(stateEstimator_t* self, uint16_t position) {
	double dt;
	double deltaPosition;
	uint32_t now;

	now = HAL_GetTick(); // in milliseconds
	dt = (double)(now - self->lastTime) * 0.001; // dt in seconds
	self->lastTime = now;

	if (dt <= 0) {
		return;
	}

	deltaPosition = (double)(position) - (double)(self->position);
	if (deltaPosition > (double)ENC_MIDDLE) deltaPosition -= (double)ENC_MAX;
	if (deltaPosition < -(double)ENC_MIDDLE) deltaPosition += (double)ENC_MAX;

	self->velocity = deltaPosition / dt;
	self->position = position;
}

void stateEstimator_setVelocityDemand(stateEstimator_t* self, double velocity) {
	self->velocityDemand = velocity;
}
