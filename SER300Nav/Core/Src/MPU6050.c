/*
 * MPU6050.c
 *
 *  Created on: Apr 10, 2020
 *      Author: shaun
 */

#include "MPU6050.h"

void MPU6050_construct(MPU6050_t* self,
		I2C_HandleTypeDef* phi2c) {
	self->phi2c = phi2c;
	self->address = MPU6050_ADDR;
	self->period = 33;
}


void MPU6050_init(MPU6050_t* self) {
	// Configure MPU6050
	uint8_t timeout = 200;
	uint8_t dataLength = 5;
	uint8_t data[dataLength];

	data[0] = SMPRT_DIV;
	data[1] = SMPRT_1000HZ;
	data[2] = CONFIG_DISABLE_ALL;
	data[3] = GYRO_CONFIG_FULL_SCALE_250;
	data[4] = ACCEL_CONFIG_FULL_SCALE_2;

	if (HAL_I2C_Master_Transmit(self->phi2c, self->address, data, dataLength, timeout) != HAL_OK) {
		MPU6050_error(self);
	}

	dataLength = 2;
	data[0] = PWR_MGMT_1;
	data[1] = PWR_MGMT_1_PLL_X_AXIS_GYRO_AND_SLEEP_DISABLE;
	if (HAL_I2C_Master_Transmit(self->phi2c, self->address, data, dataLength, timeout) != HAL_OK) {
		MPU6050_error(self);
	}

	dataLength = 2;
	data[0] = INT_PIN_CFG;
	data[1] = INT_PIN_CFG_I2C_BYPASS_EN;
	if (HAL_I2C_Master_Transmit(self->phi2c, self->address, data, dataLength, timeout) != HAL_OK) {
		MPU6050_error(self);
	}

	// Test MPU is present
	dataLength = 1;
	data[0] = WHO_AM_I;
	if (HAL_I2C_Master_Transmit(self->phi2c, self->address, data, dataLength, timeout) != HAL_OK) {
		MPU6050_error(self);
	}
	if (HAL_I2C_Master_Receive(self->phi2c, self->address, data, dataLength, timeout) != HAL_OK) {
		MPU6050_error(self);
	}
	if (data[0] << 1 != self->address) {
		MPU6050_error(self); // WHO_AM_I failed
	}
}

void MPU6050_read(MPU6050_t* self) {
	uint8_t timeout = 100;
	uint8_t dataLength = 14;
	uint8_t data[dataLength], rawReading[14];

	dataLength = 1;
	data[0] = ACCEL_XOUT_H;
	if (HAL_I2C_Master_Transmit(self->phi2c, self->address, data, dataLength, timeout) != HAL_OK) {
		MPU6050_error(self);
	}

	dataLength = 14;
	if (HAL_I2C_Master_Receive(self->phi2c, self->address, rawReading, dataLength, timeout) != HAL_OK) {
		MPU6050_error(self);
	}

	self->accX = 	rawReading[0] << 8  | rawReading[1];
	self->accY = 	rawReading[2] << 8  | rawReading[3];
	self->accZ = 	rawReading[4] << 8  | rawReading[5];
	self->tempRaw = rawReading[6] << 8  | rawReading[7];
	self->gyroX = 	rawReading[8] << 8  | rawReading[9];
	self->gyroY = 	rawReading[10] << 8 | rawReading[11];
	self->gyroZ = 	rawReading[12] << 8 | rawReading[13];
}

void MPU6050_readDMA(MPU6050_t* self) {
	uint8_t timeout = 100;
	uint8_t dataLength = 14;
	uint8_t data[dataLength];

	dataLength = 1;
	data[0] = ACCEL_XOUT_H;
	if (HAL_I2C_Master_Transmit(self->phi2c, self->address, data, dataLength, timeout) != HAL_OK) {
		MPU6050_error(self);
	}

	dataLength = 14;
	if (HAL_I2C_Master_Receive_DMA(self->phi2c, self->address, self->rxBuffer, dataLength) != HAL_OK) {
		MPU6050_error(self);
	}
}

void MPU6050_processMeasurement(MPU6050_t* self, uint8_t* rawReading) {
	self->accX = 	rawReading[0] << 8  | rawReading[1];
	self->accY = 	rawReading[2] << 8  | rawReading[3];
	self->accZ = 	rawReading[4] << 8  | rawReading[5];
	self->tempRaw = rawReading[6] << 8  | rawReading[7];
	self->gyroX = 	rawReading[8] << 8  | rawReading[9];
	self->gyroY = 	rawReading[10] << 8 | rawReading[11];
	self->gyroZ = 	rawReading[12] << 8 | rawReading[13];
}

void MPU6050_error(MPU6050_t* self) {

}

#define ADDRESS_MASK 0xFE
void MPU6050_onRxCplt(MPU6050_t* self, uint8_t address) {
	if((address & ADDRESS_MASK) == self->address) {
		MPU6050_setCommState(self, TX_COMPLETE);
	}
}

void MPU6050_setCommState(MPU6050_t* self, commState_t state) {
	self->commState = state;
}

uint8_t MPU6050_commCheck(MPU6050_t* self) {
	uint32_t now = HAL_GetTick();
	switch(self->commState) {
	case NOT_READY:			// check if we're due for another reading. Count down.
		if(now >= self->readTime) {
			MPU6050_setCommState(self, TX_READY);
			self->readTime = now + self->period;
		}
		break;
	case TX_READY:			// start transmission
		if (comm_getLock(self->address) == LOCK_OK) {
			MPU6050_readDMA(self);
			self->commState = TX_IN_PROGRESS;
		}
		break;
	case TX_IN_PROGRESS:	// do nothing
		break;
	case TX_COMPLETE:		// process measurement
		MPU6050_processMeasurement(self, self->rxBuffer);
		MPU6050_setCommState(self, NOT_READY);
		if (comm_putLock(self->address) == LOCK_ERROR) {
			I2C_LockAddress = 0;
		}
		return 1;
		break;
	}
	return 0;
}


