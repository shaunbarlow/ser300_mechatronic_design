/*
 * navigation.c
 *
 *  Created on: Apr 13, 2020
 *      Author: shaun
 */

#include "navigation.h"

void nav_construct(navigation_t* self, GY87_t* imu,
		double kp, double ki, double kd,
		double minOutput, double maxOutput) {
	self->imu = imu;
	double maxIntegralError = 0;
	pid_construct(&self->headingPID, kp, ki, kd,
			maxOutput, minOutput, maxIntegralError);

	pid_init(&self->headingPID);
}

/**
 * @brief	Generate velocities required to steer towards a heading.
 */
double nav_steerToHeading(navigation_t* self, double target) {
	double headingError;
	double rightVelocity;
	self->currentHeading = self->imu->yaw;
	headingError = nav_getSmallestAngleBetween(target, self->currentHeading);
	pid_set(&self->headingPID, 0);
	rightVelocity = pid_run(&self->headingPID, headingError);
	self->leftVelocityOutput = -rightVelocity;
	self->rightVelocityOutput = rightVelocity;
	return rightVelocity;
}


double nav_getSmallestAngleBetween(double target, double origin) {
	target = wrapAngle(target);
	origin = wrapAngle(origin);

	double angleBetween;
	angleBetween = target - origin;
	angleBetween = wrapAngle(angleBetween);
	return angleBetween;
}

/*
 * @brief	Wrap angle to range -180:180
 */
double wrapAngle(double angle) {
	while (angle > 180) angle -= 360;
	while (angle < -180) angle += 360;
	return angle;
}

uint8_t testAngles() {
	double origin;
	double target;
	double expected;
	double result;
	uint8_t failed = 0;

	origin = 10;
	target = 20;
	result = nav_getSmallestAngleBetween(target, origin);
	expected = 10;
	if (result != expected) {
		failed++;
	}

	origin = 20;
	target = 10;
	result = nav_getSmallestAngleBetween(target, origin);
	expected = -10;
	if (result != expected) {
		failed++;
	}

	origin = -170;
	target = 170;
	result = nav_getSmallestAngleBetween(target, origin);
	expected = -20;
	if (result != expected) {
		failed++;
	}

	origin = 170;
	target = -170;
	result = nav_getSmallestAngleBetween(target, origin);
	expected = 20;
	if (result != expected) {
		failed++;
	}

	return failed;
}
