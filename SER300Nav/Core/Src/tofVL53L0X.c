/*
 * TOF_VL53L0X.c
 *
 *  Created on: Apr 25, 2020
 *      Author: shaun
 */

#include <tofVL53L0X.h>

uint8_t numTOFs;
tofVL53L0X_t* tofs[10];

void tof_construct(tofVL53L0X_t* self, I2C_HandleTypeDef* phi2c, uint8_t address,
		GPIO_TypeDef* xshutPort, uint16_t xshutPin,
		GPIO_TypeDef* gpio1Port, uint16_t gpio1Pin,
		IRQn_Type interruptNumber) {
	self->Dev = &self->vl53l0x_c;
	self->Dev->I2cHandle = phi2c;
	self->Dev->I2cDevAddr = TOF_DEFAULT_ADDRESS;

	self->phi2c = phi2c;

	self->xshutPort = xshutPort;
	self->xshutPin = xshutPin;
	self->gpio1Port = gpio1Port;
	self->gpio1Pin = gpio1Pin;
	self->IRQn = interruptNumber;
	self->alpha = 0.5;

	numTOFs++;
	self->tofID = numTOFs;
	tofs[self->tofID] = self;
	if (address == TOF_DEFAULT_ADDRESS) {
		self->address = address + (self->tofID * 2);
	} else {
		self->address = address;
	}


	HAL_GPIO_WritePin(self->xshutPort, self->xshutPin, GPIO_PIN_RESET); // Disable XSHUT
}

/**
 * @brief	Init TOF sensor and start continuous ranging
 */
void tof_init(tofVL53L0X_t* self) {
	VL53L0X_Error status = 0;

	// VL53L0X init variables
	uint32_t refSpadCount;
	uint8_t isApertureSpads;
	uint8_t VhvSettings;
	uint8_t PhaseCal;

	HAL_GPIO_WritePin(self->xshutPort, self->xshutPin, GPIO_PIN_RESET); // Disable XSHUT
	HAL_Delay(20);
	HAL_GPIO_WritePin(self->xshutPort, self->xshutPin, GPIO_PIN_SET); // Enable XSHUT
	HAL_Delay(20);


	HAL_NVIC_DisableIRQ(self->IRQn);

	// NOT IMPLEMENTED status = VL53L0X_WaitDeviceBooted( self->Dev );

	if (numTOFs > 1) {
		status |= VL53L0X_SetDeviceAddress(self->Dev, self->address);
		self->Dev->I2cDevAddr = self->address;
	} else {
		self->address = TOF_DEFAULT_ADDRESS;
	}

	status |= VL53L0X_DataInit( self->Dev );
	status |= VL53L0X_StaticInit( self->Dev );
	status |= VL53L0X_PerformRefCalibration(self->Dev, &VhvSettings, &PhaseCal);
	status |= VL53L0X_PerformRefSpadManagement(self->Dev, &refSpadCount, &isApertureSpads);
	status |= VL53L0X_SetDeviceMode(self->Dev, VL53L0X_DEVICEMODE_CONTINUOUS_RANGING);
	status |= VL53L0X_StartMeasurement(self->Dev);

	HAL_NVIC_EnableIRQ(self->IRQn);
}

uint16_t tof_getDistance(tofVL53L0X_t* self) {
	self->distance = self->RangingData.RangeMilliMeter;
	return self->distance;
}

void tof_continuousMeasurementInterrupt(tofVL53L0X_t* self, uint16_t pin) {
	if(pin == self->gpio1Pin)
	{
		self->dataWaiting = 1;
//		VL53L0X_GetRangingMeasurementData(self->Dev, &self->RangingData);
//		VL53L0X_ClearInterruptMask(self->Dev, VL53L0X_REG_SYSTEM_INTERRUPT_GPIO_NEW_SAMPLE_READY);
//		self->dataReady = 1;
	}
}

void tof_DMA_continuousMeasurementInterrupt(tofVL53L0X_t* self, uint16_t pin) {
	if(pin == self->gpio1Pin)
	{
		VL53L0X_Error Status = VL53L0X_DMA_ReadMulti(self->Dev, 0x14, self->rxBuffer, 12);
		self->dataWaiting = 1;
	}
}

void tof_DMA_getContinuousMeasurement(tofVL53L0X_t* self) {
	VL53L0X_Error Status = VL53L0X_DMA_ReadMulti(self->Dev, 0x14, self->rxBuffer, 12);
}

void tof_receiveMeasurement(tofVL53L0X_t* self) {
	if (self->dataWaiting) {
		VL53L0X_GetRangingMeasurementData(self->Dev, &self->RangingData);
		VL53L0X_ClearInterruptMask(self->Dev, VL53L0X_REG_SYSTEM_INTERRUPT_GPIO_NEW_SAMPLE_READY);
		self->dataReady = 1;
		self->dataWaiting = 0;
	}
}

void tof_DMA_receiveMeasurement(tofVL53L0X_t* self) {
	if (self->dataWaiting) {
//		VL53L0X_GetRangingMeasurementData(self->Dev, &self->RangingData);
		VL53L0X_DMA_GetRangingMeasurementData(self->Dev, &self->RangingData, self->rxBuffer);
		VL53L0X_ClearInterruptMask(self->Dev, VL53L0X_REG_SYSTEM_INTERRUPT_GPIO_NEW_SAMPLE_READY);
		self->dataReady = 1;
		self->dataWaiting = 0;
	}
}

void tof_processMeasurement(tofVL53L0X_t* self) {
	VL53L0X_DMA_GetRangingMeasurementData(self->Dev, &self->RangingData, self->rxBuffer);
}

void tof_reset(tofVL53L0X_t* self) {
	VL53L0X_ClearInterruptMask(self->Dev, VL53L0X_REG_SYSTEM_INTERRUPT_GPIO_NEW_SAMPLE_READY);
}

uint16_t tof_getLowPassDistance(tofVL53L0X_t* self) {
	uint16_t newDistance = tof_getDistance(self);
	self->lowPassDistance = newDistance * self->alpha + self->lowPassDistance * (1 - self->alpha);
	return self->lowPassDistance;
}

/**
 * @brief	Fill out tofList with address of list of TOF objects.
 * @returns Number of TOFs in list.
 */
uint8_t tof_getList(tofVL53L0X_t* tofList) {
	tofList = tofs;
	return numTOFs;
}

void tof_setCommState(tofVL53L0X_t* self, commState_t state) {
	self->commState = state;
}

void tof_onExti(tofVL53L0X_t* self, uint16_t pin) {
	if(pin == self->gpio1Pin) {
		tof_setCommState(self, TX_READY);
	}
}

#define ADDRESS_MASK 0xFE
void tof_onRxCplt(tofVL53L0X_t* self, uint8_t address) {
	if((address & ADDRESS_MASK) == self->address) {
		tof_setCommState(self, TX_COMPLETE);
	}
}

void tof_commCheck(tofVL53L0X_t* self) {
	switch(self->commState) {
	case NOT_READY:  		// do nothing
		break;
	case TX_READY:			// Start transmission
		if (comm_getLock(self->address) == LOCK_OK) {
			tof_DMA_getContinuousMeasurement(self);
			self->commState = TX_IN_PROGRESS;
		}
		break;
	case TX_IN_PROGRESS:	// do nothing
		break;
	case TX_COMPLETE:		// Reset TOF
		tof_processMeasurement(self);
		tof_reset(self);
		self->commState = NOT_READY;
		if (comm_putLock(self->address) == LOCK_ERROR) {
			I2C_LockAddress = 0;
		}
		break;
	}
}
