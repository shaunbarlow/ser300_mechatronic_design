/*
 * kalman.c
 *
 *  Created on: 11 Apr 2020
 *      Author: shaun
 *
 *  Adapted from C++ implementation by:
 *   Kristian Lauszus, TKJ Electronics
 *	 Web      :  http://www.tkjelectronics.com
 *	 e-mail   :  kristianl@tkjelectronics.com
 */


#include "kalman.h"

void kalman_construct(kalman_t* self) {
	/* We will set the variables like so, these can also be tuned by the user */
	self->Q_angle = 0.001;
	self->Q_bias = 0.003;
	self->R_measure = 0.03;

	self->angle = 0; // Reset the angle
	self->bias = 0; // Reset bias

	self->P[0][0] = 0; // Since we assume that the bias is 0 and we know the starting angle (use setAngle), the error covariance matrix is set like so - see: http://en.wikipedia.org/wiki/Kalman_filter#Example_application.2C_technical
	self->P[0][1] = 0;
	self->P[1][0] = 0;
	self->P[1][1] = 0;
}

// The angle should be in degrees and the rate should be in degrees per second and the delta time in seconds
double kalman_getAngle(kalman_t* self, double newAngle, double newRate, double dt) {
	// KasBot V2  -  Kalman filter module - http://www.x-firm.com/?page_id=145
	// Modified by Kristian Lauszus
	// See my blog post for more information: http://blog.tkjelectronics.dk/2012/09/a-practical-approach-to-kalman-filter-and-how-to-implement-it

	// Discrete Kalman filter time update equations - Time Update ("Predict")
	// Update xhat - Project the state ahead
	/* Step 1 */
	self->rate = newRate - self->bias;
	self->angle += dt * self->rate;

	// Update estimation error covariance - Project the error covariance ahead
	/* Step 2 */
	self->P[0][0] += dt * (dt*self->P[1][1] - self->P[0][1] - self->P[1][0] + self->Q_angle);
	self->P[0][1] -= dt * self->P[1][1];
	self->P[1][0] -= dt * self->P[1][1];
	self->P[1][1] += self->Q_bias * dt;

	// Discrete Kalman filter measurement update equations - Measurement Update ("Correct")
	// Calculate Kalman gain - Compute the Kalman gain
	/* Step 4 */
	self->S = self->P[0][0] + self->R_measure;
	/* Step 5 */
	self->K[0] = self->P[0][0] / self->S;
	self->K[1] = self->P[1][0] / self->S;

	// Calculate angle and bias - Update estimate with measurement zk (newAngle)
	/* Step 3 */
	self->y = newAngle - self->angle;
	/* Step 6 */
	self->angle += self->K[0] * self->y;
	self->bias += self->K[1] * self->y;

	// Calculate estimation error covariance - Update the error covariance
	/* Step 7 */
	self->P[0][0] -= self->K[0] * self->P[0][0];
	self->P[0][1] -= self->K[0] * self->P[0][1];
	self->P[1][0] -= self->K[1] * self->P[0][0];
	self->P[1][1] -= self->K[1] * self->P[0][1];

	return self->angle;
}

void kalman_setAngle(kalman_t* self, double newAngle) 		{ self->angle = newAngle; }; // Used to set angle, this should be set as the starting angle
double kalman_getRate(kalman_t* self) 						{ return self->rate; }; // Return the unbiased rate

/* These are used to tune the Kalman filter */
void kalman_setQangle(kalman_t* self, double newQ_angle) {
	self->Q_angle = newQ_angle;
}
void kalman_setQbias(kalman_t* self, double newQ_bias) {
	self->Q_bias = newQ_bias;
}

void kalman_setRmeasure(kalman_t* self, double newR_measure) {
	self->R_measure = newR_measure;
}

double kalman_getQangle(kalman_t* self) {
	return self->Q_angle;
}

double kalman_getQbias(kalman_t* self) {
	return self->Q_bias;
}

double kalman_getRmeasure(kalman_t* self) {
	return self->R_measure;
}

