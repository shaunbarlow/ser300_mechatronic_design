/*
 * HMC5883L.c
 *
 *  Created on: 10 Apr 2020
 *      Author: shaun
 */

#include "HMC5883L.h"

#define MAG0MAX 603
#define MAG0MIN -578

#define MAG1MAX 542
#define MAG1MIN -701

#define MAG2MAX 547
#define MAG2MIN -556

void HMC5883L_construct(HMC5883L_t* self,
		I2C_HandleTypeDef* phi2c) {
	self->phi2c = phi2c;
	self->address = HMC5883L_ADDR;
	self->timeout = 200;
	self->offsets[0] = (MAG0MAX + MAG0MIN) / 2.;
	self->offsets[1] = (MAG1MAX + MAG1MIN) / 2.;
	self->offsets[2] = (MAG2MAX + MAG2MIN) / 2.;
	self->period = 33;
}

void HMC5883L_init(HMC5883L_t* self) {
	// Configure HMC5883L

	uint8_t dataLength = 2;
	uint8_t data[dataLength];

	data[0] = MODE;
	data[1] = MODE_CONTINUOUS;

	if (HAL_I2C_Master_Transmit(self->phi2c, self->address,
			data, dataLength, self->timeout) != HAL_OK) {
		HMC5883L_error(self);
	}

	data[0] = CONFIG_A;
	data[1] = CONFIG_A_15HZ | CONFIG_A_BIAS_NONE | CONFIG_A_8AVERAGE;

	if (HAL_I2C_Master_Transmit(self->phi2c, self->address, data, dataLength, self->timeout) != HAL_OK) {
		HMC5883L_error(self);
	}

	HMC5883L_calibrate(self);
}

void HMC5883L_read(HMC5883L_t* self) {
	// Read HMC5883L

	uint8_t dataLength = 1;
	uint8_t data[dataLength], rawReading[6];

	data[0] = DATA_OUTPUT_X_MSB;

	if (HAL_I2C_Master_Transmit(self->phi2c, self->address, data, dataLength, self->timeout) != HAL_OK) {
		HMC5883L_error(self);
	}

	dataLength = 6;
	if (HAL_I2C_Master_Receive(self->phi2c, self->address, rawReading, dataLength, self->timeout) != HAL_OK) {
		HMC5883L_error(self);
	}

	self->x = rawReading[0] << 8 | rawReading[1];
	self->z = rawReading[2] << 8 | rawReading[3];
	self->y = rawReading[4] << 8 | rawReading[5];

}

void HMC5883L_readDMA(HMC5883L_t* self) {
	// Read HMC5883L
	uint8_t dataLength = 1;
	uint8_t data[dataLength];

	data[0] = DATA_OUTPUT_X_MSB;

	if (HAL_I2C_Master_Transmit(self->phi2c, self->address, data, dataLength, self->timeout) != HAL_OK) {
		HMC5883L_error(self);
	}

	dataLength = 6;
	if (HAL_I2C_Master_Receive_DMA(self->phi2c, self->address, self->rxBuffer, dataLength) != HAL_OK) {
		HMC5883L_error(self);
	}
}

void HMC5883L_processMeasurement(HMC5883L_t* self, uint8_t* rawReading) {
	self->x = rawReading[0] << 8 | rawReading[1];
	self->z = rawReading[2] << 8 | rawReading[3];
	self->y = rawReading[4] << 8 | rawReading[5];
}

double HMC5883L_getYaw(HMC5883L_t* self) {
	self->x *= -1;
	self->z *= -1;

	self->x *= self->gains[0];
	self->y *= self->gains[1];
	self->z *= self->gains[2];

	self->x -= self->offsets[0];
	self->y *= self->offsets[1];
	self->z *= self->offsets[2];

	return self->yaw;
}

void HMC5883L_calibrate(HMC5883L_t* self) {
	// Calibrate HMC5883L


	uint8_t dataLength = 2;
	uint8_t data[dataLength];

	// Turn on positive bias
	data[0] = CONFIG_A;
	data[1] = CONFIG_A_15HZ | CONFIG_A_BIAS_POS;

	if (HAL_I2C_Master_Transmit(self->phi2c, self->address, data, dataLength, self->timeout) != HAL_OK) {
		HMC5883L_error(self);
	}

	HAL_Delay(100); // Wait for sensor
	HMC5883L_read(self);
	int16_t magPosOff[3] = {self->x,
			self->y,
			self->z };

	// Turn on negative bias
	data[1] = CONFIG_A_15HZ | CONFIG_A_BIAS_NEG;
	if (HAL_I2C_Master_Transmit(self->phi2c, self->address, data, dataLength, self->timeout) != HAL_OK) {
		HMC5883L_error(self);
	}

	HAL_Delay(100); // Wait for sensor
	HMC5883L_read(self);
	int16_t magNegOff[3] = {self->x,
			self->y,
			self->z };

	// Turn off bias. Back to normal mode.
	data[1] = CONFIG_A_15HZ | CONFIG_A_8AVERAGE | CONFIG_A_BIAS_NONE;
	if (HAL_I2C_Master_Transmit(self->phi2c, self->address, data, dataLength, self->timeout) != HAL_OK) {
		HMC5883L_error(self);
	}

	// calculate gains
	for (uint8_t i = 0; i < 3; i++) {
		self->gains[i] = (double)-2500 / (double)(magNegOff[i] - magPosOff[i]);
	}
}

void HMC5883L_error(HMC5883L_t* self) {

}

void HMC5883L_setCommState(HMC5883L_t* self, commState_t state) {
	self->commState = state;
}

#define ADDRESS_MASK 0xFE
void HMC5883L_onRxCplt(HMC5883L_t* self, uint8_t address) {
	if((address & ADDRESS_MASK) == self->address) {
		HMC5883L_setCommState(self, TX_COMPLETE);
	}
}

uint8_t HMC5883L_commCheck(HMC5883L_t* self) {
	uint32_t now = HAL_GetTick();
	switch(self->commState) {
	case NOT_READY:			// check if we're due for another reading. Count down
		if(now >= self->readTime) {
			HMC5883L_setCommState(self, TX_READY);
			self->readTime = now + self->period;
		}
		break;
	case TX_READY:			// start transmission
		if (comm_getLock(self->address) == LOCK_OK) {
			HMC5883L_readDMA(self);
			self->commState = TX_IN_PROGRESS;
		}
		break;
	case TX_IN_PROGRESS:	// do nothing
		break;
	case TX_COMPLETE:		// process measurement
		HMC5883L_processMeasurement(self, self->rxBuffer);
		HMC5883L_setCommState(self, NOT_READY);
		if (comm_putLock(self->address) == LOCK_ERROR) {
			I2C_LockAddress = 0;
		}
		return 1;
		break;
	}
	return 0;
}
