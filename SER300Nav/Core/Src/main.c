/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "GY87.h"
#include "motordriver.h"
#include "encoder.h"
#include "pid.h"
#include "stateEstimator.h"
#include "motorcontrol.h"
#include "navigation.h"
#include "tofVL53L0X.h"
#include "behaviour.h"
#include "pointCloud.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define FORWARD_VELOCITY 2000
#define DIAGONAL_DISTANCE 6000
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;
DMA_HandleTypeDef hdma_i2c1_rx;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim4;
TIM_HandleTypeDef htim5;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_I2C1_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM4_Init(void);
static void MX_TIM5_Init(void);
static void MX_TIM3_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
GY87_t imu;
motor_t motorLeft, motorRight;
encoder_t encoderLeft, encoderRight;
pidControl_t pidLeft, pidRight;
stateEstimator_t stateEstimatorLeft, stateEstimatorRight;
motorControl_t motorControlLeft, motorControlRight;
navigation_t navigation;
tofVL53L0X_t tofBottomLeft, tofBottomRight, tofTopFront, tofTopSide;

double heading;
double targetHeading;
double navResult;
double leftTofError;
double startHeading;

void returnHome();

void motorControllerISR() {
	if (mode != mode_init) {
//	HAL_GPIO_WritePin(GPIOA, LED_R_Pin, GPIO_PIN_SET);
		motorControl_run(&motorControlLeft);
		motorControl_run(&motorControlRight);
	}

}

void readProcessSensorsISR() {
	GY87_update(&imu);
	tof_commCheck(&tofBottomLeft);
	tof_commCheck(&tofBottomRight);
	tof_commCheck(&tofTopSide);
	tof_commCheck(&tofTopFront);
//	HAL_GPIO_WritePin(GPIOA, LED_R_Pin, GPIO_PIN_RESET);
}


void tofTest() {
	tof_construct(&tofBottomLeft, &hi2c1, TOF_DEFAULT_ADDRESS,
			VL53L0Xa_XSHUT_GPIO_Port, VL53L0Xa_XSHUT_Pin,
			VL53L0Xa_GPIO1_GPIO_Port, VL53L0Xa_GPIO1_Pin,
			EXTI1_IRQn);
	tof_construct(&tofTopFront, &hi2c1, TOF_DEFAULT_ADDRESS,
			VL53L0Xb_XSHUT_GPIO_Port, VL53L0Xb_XSHUT_Pin,
			VL53L0Xb_GPIO1_GPIO_Port, VL53L0Xb_GPIO1_Pin,
			EXTI2_IRQn);
	tof_construct(&tofBottomRight, &hi2c1, TOF_DEFAULT_ADDRESS,
			VL53L0Xc_XSHUT_GPIO_Port, VL53L0Xc_XSHUT_Pin,
			VL53L0Xc_GPIO1_GPIO_Port, VL53L0Xc_GPIO1_Pin,
			EXTI4_IRQn);
	tof_construct(&tofTopSide, &hi2c1, TOF_DEFAULT_ADDRESS,
			VL53L0Xd_XSHUT_GPIO_Port, VL53L0Xd_XSHUT_Pin,
			VL53L0Xd_GPIO1_GPIO_Port, VL53L0Xd_GPIO1_Pin,
			EXTI9_5_IRQn);

	tof_init(&tofBottomLeft);
	tof_init(&tofTopSide);
	tof_init(&tofBottomRight);
	tof_init(&tofTopFront);

	uint16_t distanceBR;
	uint16_t distanceBL;
	uint16_t distanceFront;
	uint16_t distanceSide;

	while (1) {
		tof_receiveMeasurement(&tofBottomRight);
		tof_receiveMeasurement(&tofBottomLeft);
		tof_receiveMeasurement(&tofTopFront);
		tof_receiveMeasurement(&tofTopSide);

		distanceBR = tof_getLowPassDistance(&tofBottomRight);
		distanceBL = tof_getLowPassDistance(&tofBottomLeft);
		distanceFront = tof_getLowPassDistance(&tofTopFront);
		distanceSide = tof_getLowPassDistance(&tofTopSide);
	}
}

void tofDMATest() {
	tof_construct(&tofBottomLeft, &hi2c1, TOF_DEFAULT_ADDRESS,
			VL53L0Xa_XSHUT_GPIO_Port, VL53L0Xa_XSHUT_Pin,
			VL53L0Xa_GPIO1_GPIO_Port, VL53L0Xa_GPIO1_Pin,
			EXTI1_IRQn);
	tof_construct(&tofTopFront, &hi2c1, TOF_DEFAULT_ADDRESS,
			VL53L0Xb_XSHUT_GPIO_Port, VL53L0Xb_XSHUT_Pin,
			VL53L0Xb_GPIO1_GPIO_Port, VL53L0Xb_GPIO1_Pin,
			EXTI2_IRQn);
	tof_construct(&tofBottomRight, &hi2c1, TOF_DEFAULT_ADDRESS,
			VL53L0Xc_XSHUT_GPIO_Port, VL53L0Xc_XSHUT_Pin,
			VL53L0Xc_GPIO1_GPIO_Port, VL53L0Xc_GPIO1_Pin,
			EXTI4_IRQn);
	tof_construct(&tofTopSide, &hi2c1, TOF_DEFAULT_ADDRESS,
			VL53L0Xd_XSHUT_GPIO_Port, VL53L0Xd_XSHUT_Pin,
			VL53L0Xd_GPIO1_GPIO_Port, VL53L0Xd_GPIO1_Pin,
			EXTI9_5_IRQn);

	tof_init(&tofBottomLeft);
	tof_init(&tofTopSide);
	tof_init(&tofBottomRight);
	tof_init(&tofTopFront);

	uint16_t distanceBR;
	uint16_t distanceBL;
	uint16_t distanceFront;
	uint16_t distanceSide;

	while (1) {
		tof_commCheck(&tofBottomLeft);
		tof_commCheck(&tofBottomRight);
		tof_commCheck(&tofTopSide);
		tof_commCheck(&tofTopFront);
//		tof_DMA_receiveMeasurement(&tofBottomLeft);
		distanceBL = tof_getLowPassDistance(&tofBottomLeft);
		distanceBR = tof_getLowPassDistance(&tofBottomRight);
		distanceFront = tof_getLowPassDistance(&tofTopFront);
		distanceSide = tof_getLowPassDistance(&tofTopSide);
	}
}

void imuDMATest() {
	GY87_construct(&imu, &hi2c1);
	GY87_init(&imu);

	while(1) {
		GY87_update(&imu);
	}
}

void I2C_DMATest() {
	tof_construct(&tofBottomLeft, &hi2c1, TOF_DEFAULT_ADDRESS,
				VL53L0Xa_XSHUT_GPIO_Port, VL53L0Xa_XSHUT_Pin,
				VL53L0Xa_GPIO1_GPIO_Port, VL53L0Xa_GPIO1_Pin,
				EXTI1_IRQn);
		tof_construct(&tofTopFront, &hi2c1, TOF_DEFAULT_ADDRESS,
				VL53L0Xb_XSHUT_GPIO_Port, VL53L0Xb_XSHUT_Pin,
				VL53L0Xb_GPIO1_GPIO_Port, VL53L0Xb_GPIO1_Pin,
				EXTI2_IRQn);
		tof_construct(&tofBottomRight, &hi2c1, TOF_DEFAULT_ADDRESS,
				VL53L0Xc_XSHUT_GPIO_Port, VL53L0Xc_XSHUT_Pin,
				VL53L0Xc_GPIO1_GPIO_Port, VL53L0Xc_GPIO1_Pin,
				EXTI4_IRQn);
		tof_construct(&tofTopSide, &hi2c1, TOF_DEFAULT_ADDRESS,
				VL53L0Xd_XSHUT_GPIO_Port, VL53L0Xd_XSHUT_Pin,
				VL53L0Xd_GPIO1_GPIO_Port, VL53L0Xd_GPIO1_Pin,
				EXTI9_5_IRQn);

		GY87_construct(&imu, &hi2c1);

		GY87_init(&imu);

		tof_init(&tofBottomLeft);
		tof_init(&tofTopSide);
		tof_init(&tofBottomRight);
		tof_init(&tofTopFront);

		uint16_t distanceBR;
		uint16_t distanceBL;
		uint16_t distanceFront;
		uint16_t distanceSide;

		while (1) {
			HAL_GPIO_WritePin(GPIOA, LED_R_Pin, GPIO_PIN_SET);
			GY87_update(&imu);
			tof_commCheck(&tofBottomLeft);
			tof_commCheck(&tofBottomRight);
			tof_commCheck(&tofTopSide);
			tof_commCheck(&tofTopFront);
	//		tof_DMA_receiveMeasurement(&tofBottomLeft);
			distanceBL = tof_getLowPassDistance(&tofBottomLeft);
			distanceBR = tof_getLowPassDistance(&tofBottomRight);
			distanceFront = tof_getLowPassDistance(&tofTopFront);
			distanceSide = tof_getLowPassDistance(&tofTopSide);
			HAL_GPIO_WritePin(GPIOA, LED_R_Pin, GPIO_PIN_RESET);
			HAL_Delay(1);
		}
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {
	tof_onExti(&tofBottomLeft, GPIO_Pin);
	tof_onExti(&tofBottomRight, GPIO_Pin);
	tof_onExti(&tofTopFront, GPIO_Pin);
	tof_onExti(&tofTopSide, GPIO_Pin);
}

void HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef *hi2c) {
	tof_onRxCplt(&tofBottomRight, (uint8_t)hi2c->Devaddress);
	tof_onRxCplt(&tofBottomLeft, (uint8_t)hi2c->Devaddress);
	tof_onRxCplt(&tofTopFront, (uint8_t)hi2c->Devaddress);
	tof_onRxCplt(&tofTopSide, (uint8_t)hi2c->Devaddress);
	HMC5883L_onRxCplt(&(imu.mag), (uint8_t)hi2c->Devaddress);
	MPU6050_onRxCplt(&(imu.imu), (uint8_t)hi2c->Devaddress);
}


void motorTest() {
	/*
#define DRV_LEFT_B_Pin GPIO_PIN_8
#define DRV_LEFT_B_GPIO_Port GPIOA
#define DRV_LEFT_A_Pin GPIO_PIN_9
#define DRV_LEFT_A_GPIO_Port GPIOA
#define DRV_RIGHT_B_Pin GPIO_PIN_10
#define DRV_RIGHT_B_GPIO_Port GPIOA
#define DRV_RIGHT_A_Pin GPIO_PIN_11
#define DRV_RIGHT_A_GPIO_Port GPIOA
	 */
//	motor_construct(&motorRight, &htim1, TIM_CHANNEL_1, TIM_CHANNEL_2);
//	motor_construct(&motorLeft, &htim1, TIM_CHANNEL_4, TIM_CHANNEL_3);
//	motor_init(&motorLeft);
//	motor_init(&motorRight);
	HAL_Delay(100);
	float direction = 1;
	float output = 0;
	uint8_t counter = 0;
	while(1) {
		counter++;
		if (counter == 50) break;
		output += 0.05 * direction;
		motor_setOutput(&motorRight, output);
		motor_setOutput(&motorLeft, 0.0);
		if (output >= 0.6) {
			direction = -1;
		}
		if (output <= -0.6) {
			direction = 1.0;
		}
		HAL_Delay(50);
	}
	counter = 0;
	while(1) {
		counter++;
		if (counter == 50) break;
		output += 0.04 * direction;
		motor_setOutput(&motorLeft, output);
		motor_setOutput(&motorRight, 0.0);
		if (output >= 0.6) {
			direction = -1;
		}
		if (output <= -0.6) {
			direction = 1.0;
		}
		HAL_Delay(50);
	}
}

void pointCloudTest() {
	pointCloud_t cloud;
	pointCloud_clear(&cloud);
	pointCloud_setPoint(&cloud, 1, 100);
	uint16_t result = pointCloud_getPoint(&cloud, 1);
	result;
}

void pointCloudGatherTest() {
	pointCloud_t cloud;
	pointCloud_clear(&cloud);
//	int16_t sampleCount = 0;
//	int16_t startAngle = (int16_t)imu.yaw;
//	int16_t prevAngle = (int16_t)imu.yaw;
//	stateEstimator_setVelocityDemand(&stateEstimatorLeft, 200);
//	stateEstimator_setVelocityDemand(&stateEstimatorRight, -200);
//	uint32_t endTime = HAL_GetTick() + 20000;

	for (uint16_t i = 0; i < 360; i++) {
		double targetHeading = i;
		if (targetHeading > 180) targetHeading -= 360;
		while (1) {
			uint16_t lowerDist = tof_getLowPassDistance(&tofBottomRight);
			// turn to targetHeading on the spot
			navResult = nav_steerToHeading(&navigation, targetHeading);
			double rightVel = navResult;
			double leftVel = -navResult;
			stateEstimator_setVelocityDemand(&stateEstimatorLeft, leftVel);
			stateEstimator_setVelocityDemand(&stateEstimatorRight, rightVel);
			// leave state once heading is +-1 deg
			if (fabs(navResult) < 10) {
				stateEstimator_setVelocityDemand(&stateEstimatorLeft, 0);
				stateEstimator_setVelocityDemand(&stateEstimatorRight, 0);
				pointCloud_setPoint(&cloud, i, lowerDist);
				break;
			}
		}
	}

//	while (1) {
//		int16_t newAngle = (int16_t)imu.yaw;
//		if (newAngle != prevAngle) {
//			uint16_t lowerDist = tof_getLowPassDistance(&tofBottomRight);
//			uint16_t cloudAngle;
//			if (newAngle < 0) cloudAngle = 360 + (uint16_t) newAngle;
//			else cloudAngle = (uint16_t) newAngle;
//			pointCloud_setPoint(&cloud, cloudAngle, lowerDist);
////			if (startAngle == newAngle) {
////				break;
////			}
//			sampleCount++;
//			if (sampleCount > 100) {
//				if ((newAngle > startAngle * 0.9)
//						&& (newAngle < startAngle * 1.1)){
//					break;
//				}
//			}
//			if (HAL_GetTick() > endTime) break;
//		}
//		prevAngle = newAngle;
//	}
	mode = mode_noMotors;
	HAL_Delay(1000);
	uint16_t numEdges = pointCloud_populateEdges(&cloud, 60);
	uint16_t obstacleHeading = pointCloud_findNextObstacleOfWidth(&cloud, 40);

	obstacleHeading;
	while(1) {

	}
}

enum edgeState {
	noEdgeFound,
	rightEdgeFound,
	leftEdgeFound,
	turningToBall,
	collectingBall,
	finished,
};

uint8_t state = 0;
uint16_t prevDistL;
uint16_t nowDistL;
void edgeDetect() {


	uint16_t upperDist;
	uint16_t lowerDist;
//	uint16_t prevDistL;
//	uint16_t nowDistL;
	uint16_t targetDistance;
	uint16_t distanceRemaining;
	double rightEdgeHeading, leftEdgeHeading;
	double rightVel;
	double leftVel;
	while (1) {
		switch(state) {
		case noEdgeFound:

			upperDist = tof_getLowPassDistance(&tofTopFront);
			lowerDist = tof_getLowPassDistance(&tofBottomRight);
			// turn right
			// look for edge with right TOF
			if (behaviour_pointToBall(&stateEstimatorLeft,
					&stateEstimatorRight,
					upperDist, lowerDist) == BALL_FOUND) {
				// edge found. Move to next state
				state = finished;
				rightEdgeHeading = imu.yaw;
				prevDistL = tof_getDistance(&tofBottomLeft);
			}


			break;
		case rightEdgeFound:
			nowDistL = tof_getDistance(&tofBottomLeft);
			// turn right
			// look for edge with left TOF
			if (behaviour_checkForBallDelta(prevDistL, nowDistL) == NO_BALL) {
				if ((nowDistL > lowerDist * 0.8) || (nowDistL < lowerDist * 1.2)) {
					state = leftEdgeFound;
				}
			}
			stateEstimator_setVelocityDemand(&stateEstimatorLeft, 50);
			stateEstimator_setVelocityDemand(&stateEstimatorRight, -50);
			prevDistL = nowDistL;
			break;
		case leftEdgeFound:
			// calculate angle halfway between left and right edges
			leftEdgeHeading = imu.yaw;
			// set as heading
			targetHeading = rightEdgeHeading + (0.5 * (leftEdgeHeading - rightEdgeHeading));
			// set distance to drive as distance to edge + margin
			targetDistance = encoder_getReading(&encoderLeft) + nowDistL + 150;
			state = turningToBall;
			break;
		case turningToBall:

			// turn to targetHeading on the spot
			navResult = nav_steerToHeading(&navigation, targetHeading);
			rightVel = navResult;
			leftVel = -navResult;
			stateEstimator_setVelocityDemand(&stateEstimatorLeft, leftVel);
			stateEstimator_setVelocityDemand(&stateEstimatorRight, rightVel);
			// leave state once heading is +-1 deg
			if (fabs(navResult) < 50) {
				stateEstimator_setVelocityDemand(&stateEstimatorLeft, 0);
				stateEstimator_setVelocityDemand(&stateEstimatorRight, 0);
				state = collectingBall;
			}
			break;
		case collectingBall:
			// drive toward heading
			// stop when distance is reached

			// use encoders to measure distance
			distanceRemaining = targetDistance - encoder_getReading(&encoderLeft);
			if (distanceRemaining > 0) {
				// use heading to maintain direction
				navResult = nav_steerToHeading(&navigation, targetHeading);
				rightVel = 500 + navResult;
				leftVel = 500 - navResult;
				stateEstimator_setVelocityDemand(&stateEstimatorLeft, leftVel);
				stateEstimator_setVelocityDemand(&stateEstimatorRight, rightVel);
			} else { // reached target distance
				// stop
				stateEstimator_setVelocityDemand(&stateEstimatorLeft, 0);
				stateEstimator_setVelocityDemand(&stateEstimatorRight, 0);
				state = finished;
			}
			break;
		case finished:
			// start again
			stateEstimator_setVelocityDemand(&stateEstimatorLeft, 0);
			stateEstimator_setVelocityDemand(&stateEstimatorRight, 0);
			break;
		}
	}
}


enum edgeOffset {
	TURN_45,
	PATROL,
	SEARCH_LEFT,
	SEARCH_RIGHT,
	SEARCH,
	SEARCH_360,
	CHECK,
	ORIENT,
	DRIVE,
	STOP
};
#define DEGREES(x) (x * 180 / M_PI)

void edgeOffset() {
	uint16_t upperDist;
	uint16_t lowerDist;
//	uint16_t prevDistL;
//	uint16_t nowDistL;
	uint16_t targetDistance;
	uint16_t distanceRemaining;
	double rightEdgeHeading, leftEdgeHeading;
	double rightVel;
	double leftVel;

	while(1) {
		switch(state) {
		case SEARCH:
			upperDist = tof_getLowPassDistance(&tofTopFront);
			lowerDist = tof_getLowPassDistance(&tofBottomRight);
			if (behaviour_driveToBall(&stateEstimatorLeft,
								&stateEstimatorRight,
								upperDist, lowerDist) == BALL_FOUND) {
				if (lowerDist < 20) {
					state = ORIENT;
					rightEdgeHeading = imu.yaw;
					targetHeading = rightEdgeHeading + DEGREES(fabs(atan2(50, lowerDist)));
					targetDistance = encoder_getReading(&encoderLeft) + (lowerDist * 4) + 150;
				}
			}

			break;

			// turn right
			// look for edge with right TOF
//			if (behaviour_pointToBall(&stateEstimatorLeft,
//					&stateEstimatorRight,
//					upperDist, lowerDist) == BALL_FOUND) {
//				// edge found. Move to next state
//				state = ORIENT;
//				rightEdgeHeading = imu.yaw;
//				targetHeading = rightEdgeHeading + DEGREES(fabs(atan2(50, lowerDist)));
//				targetDistance = encoder_getReading(&encoderLeft) + (lowerDist * 4) + 150;
//			}
//			break;
		case ORIENT:
			// turn to targetHeading on the spot
			navResult = nav_steerToHeading(&navigation, targetHeading);
			rightVel = navResult;
			leftVel = -navResult;
			stateEstimator_setVelocityDemand(&stateEstimatorLeft, leftVel);
			stateEstimator_setVelocityDemand(&stateEstimatorRight, rightVel);
			// leave state once heading is +-1 deg
			if (fabs(navResult) <3) {
//				stateEstimator_setVelocityDemand(&stateEstimatorLeft, 0);
//				stateEstimator_setVelocityDemand(&stateEstimatorRight, 0);
				state = DRIVE;
			}
			break;
		case DRIVE:
			// use encoders to measure distance
			distanceRemaining = targetDistance - encoder_getReading(&encoderLeft);
			if (distanceRemaining > 0) {
				// use heading to maintain direction
//				navResult = nav_steerToHeading(&navigation, targetHeading);
				navResult = 0;
				rightVel = 500 + navResult;
				leftVel = 500 - navResult;
				stateEstimator_setVelocityDemand(&stateEstimatorLeft, leftVel);
				stateEstimator_setVelocityDemand(&stateEstimatorRight, rightVel);
			} else { // reached target distance
				// stop
				stateEstimator_setVelocityDemand(&stateEstimatorLeft, 0);
				stateEstimator_setVelocityDemand(&stateEstimatorRight, 0);
				state = STOP;
			}
			break;
		case STOP:
			break;
		}
	}
}

uint16_t lowerDistMin;
void edgeCheck() {
	uint16_t upperDist;
	uint16_t lowerDist;
//	uint16_t lowerDistMin;
	//	uint16_t prevDist;
	//	uint16_t nowDist;
	uint16_t targetDistance;
	uint16_t distanceRemaining;
	uint8_t angleOfNextEdge;
	double rightEdgeHeading, leftEdgeHeading;
	double rightVel;
	double leftVel;
	startHeading = imu.yaw;

	uint8_t turned180;
	double searchStartAngle, search180Angle;
//	state = SEARCH_360;
	lowerDistMin = 9000;
	targetHeading = imu.yaw;
	searchStartAngle = imu.yaw;
	search180Angle = searchStartAngle + 180;
	if (search180Angle > 180) search180Angle -= 360;
	turned180 = 0;
	while(1) {
		upperDist = tof_getLowPassDistance(&tofTopFront);
		lowerDist = tof_getLowPassDistance(&tofBottomRight);

		switch(state) {
		case TURN_45:
			// turn to targetHeading on the spot
			navResult = nav_steerToHeading(&navigation, startHeading + 35);
			rightVel = navResult;
			leftVel = -navResult;
			stateEstimator_setVelocityDemand(&stateEstimatorLeft, leftVel);
			stateEstimator_setVelocityDemand(&stateEstimatorRight, rightVel);
			// leave state once heading is +-1 deg
			if (fabs(navResult) <3) {
				//				stateEstimator_setVelocityDemand(&stateEstimatorLeft, 0);
				//				stateEstimator_setVelocityDemand(&stateEstimatorRight, 0);
				state = PATROL;
				targetDistance = encoder_getReading(&encoderLeft) + 5000;
			}
			break;
		case PATROL:

			// use encoders to measure distance
			distanceRemaining = targetDistance - encoder_getReading(&encoderLeft);
			stateEstimator_setVelocityDemand(&stateEstimatorLeft, 1500);
			stateEstimator_setVelocityDemand(&stateEstimatorRight, 1500);
			if (distanceRemaining == 0) {
				state = SEARCH_360;
				stateEstimator_setVelocityDemand(&stateEstimatorLeft, 0);
				stateEstimator_setVelocityDemand(&stateEstimatorRight, 0);
				lowerDistMin = 9000;
				targetHeading = imu.yaw;
				searchStartAngle = imu.yaw;
				search180Angle = searchStartAngle + 180;
				if (search180Angle > 180) search180Angle -= 360;
				turned180 = 0;
			}
//			state = SEARCH;
			break;
//		case SEARCH_LEFT:
//			upperDist = tof_getLowPassDistance(&tofTopFront);
//			lowerDist = tof_getLowPassDistance(&tofBottomRight);
//			// turn right
//			// look for edge with right TOF
//			if (behaviour_checkForBallRight(upperDist, lowerDist) == BALL_FOUND) {
//				// edge found. Move to next state
//				state = STOP;
//				rightEdgeHeading = imu.yaw;
//				targetHeading = rightEdgeHeading + DEGREES(fabs(atan2(50, lowerDist)));
//				targetDistance = encoder_getReading(&encoderLeft) + (lowerDist * 4) + 150;
//				angleOfNextEdge = imu.yaw + pointCloud_getDegreesForObjectOfWidth(lowerDist, 40);
//			} else {
//				stateEstimator_setVelocityDemand(&stateEstimatorLeft, -200);
//				stateEstimator_setVelocityDemand(&stateEstimatorRight, 200);
//			}
//			break;
//		case SEARCH_RIGHT:
//			upperDist = tof_getLowPassDistance(&tofTopFront);
//			lowerDist = tof_getLowPassDistance(&tofBottomRight);
//			// turn right
//			// look for edge with right TOF
//			if (behaviour_checkForBallRight(upperDist, lowerDist) == BALL_FOUND) {
//				// edge found. Move to next state
//				state = CHECK;
//				rightEdgeHeading = imu.yaw;
//				targetHeading = rightEdgeHeading + DEGREES(fabs(atan2(50, lowerDist)));
//				targetDistance = encoder_getReading(&encoderLeft) + (lowerDist * 4) + 150;
//				angleOfNextEdge = imu.yaw + pointCloud_getDegreesForObjectOfWidth(lowerDist, 40);
//			} else {
//				stateEstimator_setVelocityDemand(&stateEstimatorLeft, 200);
//				stateEstimator_setVelocityDemand(&stateEstimatorRight, -200);
//			}
//			break;

		case SEARCH_360:
			lowerDist = tof_getLowPassDistance(&tofBottomRight);
			if (lowerDist > 20 && lowerDist < lowerDistMin) {
				lowerDistMin = lowerDist;
				targetHeading = imu.yaw;

				if (lowerDistMin < 400) {
					targetDistance = encoder_getReading(&encoderLeft) + (lowerDist * 7);
					state = ORIENT;
					targetHeading += 10;
				}
			}

			if (imu.yaw < (search180Angle * 1.5)
					&& imu.yaw > (search180Angle * 0.5)) {
				turned180 = 1;
			}
			if (turned180) {
				if (imu.yaw < (searchStartAngle + 10)
					&& imu.yaw > (searchStartAngle)) {
					targetDistance = encoder_getReading(&encoderLeft) + (lowerDist * 5);
					state = ORIENT;
				}
			}

			stateEstimator_setVelocityDemand(&stateEstimatorLeft, 300);
			stateEstimator_setVelocityDemand(&stateEstimatorRight, -300);
			break;

//		case SEARCH:
//			upperDist = tof_getLowPassDistance(&tofTopFront);
//			lowerDist = tof_getLowPassDistance(&tofBottomRight);
//			// turn right
//			// look for edge with right TOF
//			if (behaviour_driveToBall(&stateEstimatorLeft,
//					&stateEstimatorRight,
//					upperDist, lowerDist) == BALL_FOUND) {
//				if (lowerDist < 100) {
//					// edge found. Move to next state
//					state = CHECK;
//					rightEdgeHeading = imu.yaw;
//					targetHeading = rightEdgeHeading + DEGREES(fabs(atan2(50, lowerDist)));
//					targetDistance = encoder_getReading(&encoderLeft) + (lowerDist * 4) + 150;
//					angleOfNextEdge = imu.yaw + pointCloud_getDegreesForObjectOfWidth(lowerDist, 40);
//				}
//			}
//			break;
		case CHECK:
			upperDist = tof_getLowPassDistance(&tofTopFront);
			lowerDist = tof_getLowPassDistance(&tofBottomRight);

			stateEstimator_setVelocityDemand(&stateEstimatorLeft, 200);
			stateEstimator_setVelocityDemand(&stateEstimatorRight, -200);
			if (imu.yaw < (1.5 * angleOfNextEdge)) {
				if (behaviour_checkForBallRight(upperDist, lowerDist) == NO_BALL) {
					state = ORIENT;
				}
			} else {
				state = SEARCH_LEFT;
			}
			break;
		case ORIENT:
			// turn to targetHeading on the spot
			navResult = nav_steerToHeading(&navigation, targetHeading);
			rightVel = navResult;
			leftVel = -navResult;
			stateEstimator_setVelocityDemand(&stateEstimatorLeft, leftVel);
			stateEstimator_setVelocityDemand(&stateEstimatorRight, rightVel);
			// leave state once heading is +-1 deg
			if (fabs(navResult) <3) {
				//				stateEstimator_setVelocityDemand(&stateEstimatorLeft, 0);
				//				stateEstimator_setVelocityDemand(&stateEstimatorRight, 0);
				state = DRIVE;
			}
			break;
		case DRIVE:
			// use encoders to measure distance
			distanceRemaining = targetDistance - encoder_getReading(&encoderLeft);
			if (distanceRemaining > 0) {
				// use heading to maintain direction
				//				navResult = nav_steerToHeading(&navigation, targetHeading);
				navResult = 0;
				rightVel = 1000 + navResult;
				leftVel = 1000 - navResult;
				stateEstimator_setVelocityDemand(&stateEstimatorLeft, leftVel);
				stateEstimator_setVelocityDemand(&stateEstimatorRight, rightVel);
			} else { // reached target distance
				// stop
				stateEstimator_setVelocityDemand(&stateEstimatorLeft, 0);
				stateEstimator_setVelocityDemand(&stateEstimatorRight, 0);
				state = STOP;
			}
			break;
		case STOP:
			// stop
			stateEstimator_setVelocityDemand(&stateEstimatorLeft, 0);
			stateEstimator_setVelocityDemand(&stateEstimatorRight, 0);
			returnHome();
			break;
		}
	}
}


enum returnHomeState {
	TURN_180_TO_START_ANGLE,
	DRIVE_TO_WALL,
	TURN_90_TO_START_ANGLE,
	DRIVE_TO_BASE,
	FINISH
};

void returnHome() {
	uint16_t upperDist;
	double rightVel;
	double leftVel;
	state = TURN_180_TO_START_ANGLE;

	while (1) {
		switch (state) {
		case TURN_180_TO_START_ANGLE:
			// turn to targetHeading on the spot
			navResult = nav_steerToHeading(&navigation, startHeading + 180);
			rightVel = navResult;
			leftVel = -navResult;
			stateEstimator_setVelocityDemand(&stateEstimatorLeft, leftVel);
			stateEstimator_setVelocityDemand(&stateEstimatorRight, rightVel);
			// leave state once heading is +-1 deg
			if (fabs(navResult) <3) {

				state = DRIVE_TO_WALL;
			}
			break;
		case DRIVE_TO_WALL:

			navResult = nav_steerToHeading(&navigation, startHeading + 180);
			rightVel = 1000 + navResult;
			leftVel = 1000 - navResult;

			stateEstimator_setVelocityDemand(&stateEstimatorLeft, leftVel);
			stateEstimator_setVelocityDemand(&stateEstimatorRight, rightVel);

			if (tof_getLowPassDistance(&tofTopFront) < 200) {
				stateEstimator_setVelocityDemand(&stateEstimatorLeft, 0);
				stateEstimator_setVelocityDemand(&stateEstimatorRight, 0);
				state = TURN_90_TO_START_ANGLE;
			}
			break;
		case TURN_90_TO_START_ANGLE:
			// turn to targetHeading on the spot
			navResult = nav_steerToHeading(&navigation, startHeading - 90);
			rightVel = navResult;
			leftVel = -navResult;
			stateEstimator_setVelocityDemand(&stateEstimatorLeft, leftVel);
			stateEstimator_setVelocityDemand(&stateEstimatorRight, rightVel);
			// leave state once heading is +-1 deg
			if (fabs(navResult) <3) {

				state = DRIVE_TO_BASE;
			}
			tof_getLowPassDistance(&tofTopSide);
			break;
		case DRIVE_TO_BASE:
			// use heading to maintain direction
			leftTofError = ((double)tof_getLowPassDistance(&tofTopSide) - 100) * 1;
			if (leftTofError > 100) leftTofError = 100;

			stateEstimator_setVelocityDemand(&stateEstimatorLeft, 1000 - leftTofError);
			stateEstimator_setVelocityDemand(&stateEstimatorRight, 1000 + leftTofError);

			if (tof_getLowPassDistance(&tofTopFront) < 200) {
				stateEstimator_setVelocityDemand(&stateEstimatorLeft, 0);
				stateEstimator_setVelocityDemand(&stateEstimatorRight, 0);
				state = FINISH;
			}
			break;
		case FINISH:
			HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_RESET);
			break;
		}
	}
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */


	mode = mode_init;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */
/*
 * clock_freq = 84 * 10 ^ 6
 * prescaler = 8400
 * prescaled freq = 1 * 10 ^ 4
 * counter = 10 gives 1khz
 */
  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART2_UART_Init();
  MX_I2C1_Init();
  MX_TIM1_Init();
  MX_TIM4_Init();
  MX_TIM5_Init();
  MX_TIM3_Init();
  /* USER CODE BEGIN 2 */
//  testAngles();

//  tofTest();
//  tofDMATest();
//  imuDMATest();
//  I2C_DMATest();
//  motorTest();
//  pointCloudTest();

  /* Constructors */
  encoder_construct(&encoderRight, &htim4);
  encoder_construct(&encoderLeft, &htim3);
  GY87_construct(&imu, &hi2c1);
  motor_construct(&motorRight, &htim1, TIM_CHANNEL_1, TIM_CHANNEL_2);
  motor_construct(&motorLeft, &htim1, TIM_CHANNEL_4, TIM_CHANNEL_3);
  stateEstimator_construct(&stateEstimatorLeft);
  stateEstimator_construct(&stateEstimatorRight);
  tof_construct(&tofBottomLeft, &hi2c1, TOF_DEFAULT_ADDRESS,
		  VL53L0Xa_XSHUT_GPIO_Port, VL53L0Xa_XSHUT_Pin,
		  VL53L0Xa_GPIO1_GPIO_Port, VL53L0Xa_GPIO1_Pin,
		  EXTI1_IRQn);
  tof_construct(&tofTopFront, &hi2c1, TOF_DEFAULT_ADDRESS,
  		  VL53L0Xb_XSHUT_GPIO_Port, VL53L0Xb_XSHUT_Pin,
  		  VL53L0Xb_GPIO1_GPIO_Port, VL53L0Xb_GPIO1_Pin,
		  EXTI2_IRQn);
  tof_construct(&tofBottomRight, &hi2c1, TOF_DEFAULT_ADDRESS,
    		  VL53L0Xc_XSHUT_GPIO_Port, VL53L0Xc_XSHUT_Pin,
    		  VL53L0Xc_GPIO1_GPIO_Port, VL53L0Xc_GPIO1_Pin,
			  EXTI4_IRQn);
  tof_construct(&tofTopSide, &hi2c1, TOF_DEFAULT_ADDRESS,
    		  VL53L0Xd_XSHUT_GPIO_Port, VL53L0Xd_XSHUT_Pin,
    		  VL53L0Xd_GPIO1_GPIO_Port, VL53L0Xd_GPIO1_Pin,
			  EXTI9_5_IRQn);

  double kp;
  double ki;
  double kd;
  double minOutput;
  double maxOutput;
  double maxIntegralError;

  kp = 0.001;
  ki = 0.01;
  kd = 0;
  maxOutput = 1.0;
  minOutput = -1.0;
  maxIntegralError = 1000000;
  pid_construct(&pidLeft, kp, ki, kd,
		  maxOutput, minOutput, maxIntegralError);

  pid_construct(&pidRight, kp, ki, kd,
  		  maxOutput, minOutput, maxIntegralError);

  motorControl_construct(&motorControlLeft, &stateEstimatorLeft,
		  &pidLeft, &encoderLeft, &motorLeft);
  motorControl_construct(&motorControlRight, &stateEstimatorRight,
  		  &pidRight, &encoderRight, &motorRight);

  kp = 10;
  ki = 100;
  kd = 0;
  minOutput = -500;
  maxOutput = 500;
  nav_construct(&navigation, &imu, kp, ki, kd,
		  minOutput, maxOutput);

  /* Inits */
  motor_init(&motorLeft);
  motor_init(&motorRight);
//  motorTest();

  GY87_init(&imu);

  encoder_init(&encoderLeft);
  encoder_init(&encoderRight);
  motorControl_init(&motorControlLeft);
  motorControl_init(&motorControlRight);

  pid_init(&pidLeft);
  pid_init(&pidRight);

  tof_init(&tofBottomLeft);
  tof_init(&tofTopFront);
  tof_init(&tofBottomRight);
  tof_init(&tofTopSide);

  mode = mode_standby;

  HAL_TIM_Base_Start_IT(&htim5); // start the 500hz timer for wheel velocity control and sensor updates.


  HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_SET);
  HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_RESET);

  while (mode != mode_enabled) {
  }
  uint32_t startTime = HAL_GetTick() + 5000;

  HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_RESET);
  while (HAL_GetTick() < startTime) {
	  HAL_GPIO_TogglePin(LED_G_GPIO_Port, LED_G_Pin);
	  HAL_GPIO_TogglePin(LED_R_GPIO_Port, LED_R_Pin);
	  HAL_Delay(500);
  }

  HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, GPIO_PIN_SET);
  HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_RESET);


//  pointCloudGatherTest();

  edgeCheck();
//  edgeOffset();
//  edgeDetect();

  uint16_t upperDist;
  uint16_t lowerDist;
  while (1) {
	  upperDist = tof_getLowPassDistance(&tofTopFront);
	  lowerDist = tof_getLowPassDistance(&tofBottomRight);

//	  if (behaviour_checkForBall(upperDist, lowerDist) == BALL_FOUND) {
	  if (behaviour_driveToBall(&stateEstimatorLeft, &stateEstimatorRight,
	  			  upperDist, lowerDist) == BALL_FOUND) {
//		  if (behaviour_pointToBall(&stateEstimatorLeft, &stateEstimatorRight,
//				  upperDist, lowerDist) == BALL_FOUND) {
		  HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_SET);
		  HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, GPIO_PIN_RESET);
	  } else {
		  HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, GPIO_PIN_SET);
		  HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_RESET);
	  }

//	  if (behaviour_pointToBall(&stateEstimatorLeft, &stateEstimatorRight,
//			  upperDist, lowerDist) == BALL_FOUND) {
//		  mode = mode_standby;
//		  break;
//	  }
  }

  while (mode != mode_enabled) {  }

  heading = imu.yaw;
  targetHeading = heading;

  uint16_t startDistance;
  startDistance = encoder_getReading(&encoderLeft);
  uint16_t targetDistance;
  int32_t distanceRemaining;
  uint8_t step = 1;


  double rightVel;
  double leftVel;
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {

	  uint16_t distanceBR = tof_getLowPassDistance(&tofBottomRight);
	  uint16_t distanceBL = tof_getLowPassDistance(&tofBottomLeft);
	  uint16_t distanceFront = tof_getLowPassDistance(&tofTopFront);
	  uint16_t distanceSide = tof_getLowPassDistance(&tofTopSide);

	  switch (step) {
	  case 1: // first straight
	  case 3: // second straight
	  case 5: // third straight
		  // drive forward until distance < 200mm
		  if (distanceFront < 200) { // within stopping distance
			  stateEstimator_setVelocityDemand(&stateEstimatorLeft, 0);
			  stateEstimator_setVelocityDemand(&stateEstimatorRight, 0);
			  targetHeading += 90;
			  step++;
		  } else { // keep driving forward
			  // use heading to maintain direction
			  leftTofError = ((double)distanceSide - 200) * 10;
			  if (leftTofError > 100) leftTofError = 100;
//			  leftTofError = 0;
			  navResult = nav_steerToHeading(&navigation, targetHeading);
			  rightVel = FORWARD_VELOCITY + navResult + leftTofError;
			  leftVel = FORWARD_VELOCITY - navResult - leftTofError;
			  stateEstimator_setVelocityDemand(&stateEstimatorLeft, leftVel);
			  stateEstimator_setVelocityDemand(&stateEstimatorRight, rightVel);
		  }
		  break;

	  case 2: // first 90 deg turn
	  case 4: // second 90 deg turn
		  // turn to targetHeading on the spot
		  navResult = nav_steerToHeading(&navigation, targetHeading);
		  rightVel = navResult;
		  leftVel = -navResult;
		  stateEstimator_setVelocityDemand(&stateEstimatorLeft, leftVel);
		  stateEstimator_setVelocityDemand(&stateEstimatorRight, rightVel);
		  // leave state once heading is +-1 deg
		  if (fabs(navResult) < 50) {
			  stateEstimator_setVelocityDemand(&stateEstimatorLeft, 0);
			  stateEstimator_setVelocityDemand(&stateEstimatorRight, 0);
			  step++;
		  }
		  break;
	  case 6: // third turn 135 deg
		  // turn to targetHeading on the spot
		  navResult = nav_steerToHeading(&navigation, targetHeading + 45);
		  rightVel = navResult;
		  leftVel = -navResult;
		  stateEstimator_setVelocityDemand(&stateEstimatorLeft, leftVel);
		  stateEstimator_setVelocityDemand(&stateEstimatorRight, rightVel);
		  // leave state once heading is +-1 deg
		  if (fabs(navResult) < 50) {
			  stateEstimator_setVelocityDemand(&stateEstimatorLeft, 0);
			  stateEstimator_setVelocityDemand(&stateEstimatorRight, 0);
			  targetDistance = encoder_getReading(&encoderLeft) + DIAGONAL_DISTANCE;
			  targetHeading += 45;
			  step = 7;
		  }
		  break;
	  case 7:
		  // use encoders to measure distance
		  distanceRemaining = targetDistance - encoder_getReading(&encoderLeft);
		  if (distanceRemaining > 0) {
			  // use heading to maintain direction
			  navResult = nav_steerToHeading(&navigation, targetHeading);
			  rightVel = FORWARD_VELOCITY + navResult;
			  leftVel = FORWARD_VELOCITY - navResult;
			  stateEstimator_setVelocityDemand(&stateEstimatorLeft, leftVel);
			  stateEstimator_setVelocityDemand(&stateEstimatorRight, rightVel);
		  } else { // reached target distance
			  // stop
			  stateEstimator_setVelocityDemand(&stateEstimatorLeft, 0);
			  stateEstimator_setVelocityDemand(&stateEstimatorRight, 0);
			  step++;
		  }

		  break;
	  default:
		  stateEstimator_setVelocityDemand(&stateEstimatorLeft, 0);
		  stateEstimator_setVelocityDemand(&stateEstimatorRight, 0);
	  }

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 16;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 400000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 7;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 199;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_Encoder_InitTypeDef sConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 0;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 0xFFFF;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  sConfig.EncoderMode = TIM_ENCODERMODE_TI12;
  sConfig.IC1Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC1Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC1Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC1Filter = 5;
  sConfig.IC2Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC2Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC2Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC2Filter = 5;
  if (HAL_TIM_Encoder_Init(&htim3, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */

}

/**
  * @brief TIM4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM4_Init(void)
{

  /* USER CODE BEGIN TIM4_Init 0 */

  /* USER CODE END TIM4_Init 0 */

  TIM_Encoder_InitTypeDef sConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM4_Init 1 */

  /* USER CODE END TIM4_Init 1 */
  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 0;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 0xffff;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  sConfig.EncoderMode = TIM_ENCODERMODE_TI12;
  sConfig.IC1Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC1Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC1Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC1Filter = 5;
  sConfig.IC2Polarity = TIM_ICPOLARITY_FALLING;
  sConfig.IC2Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC2Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC2Filter = 5;
  if (HAL_TIM_Encoder_Init(&htim4, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM4_Init 2 */

  /* USER CODE END TIM4_Init 2 */

}

/**
  * @brief TIM5 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM5_Init(void)
{

  /* USER CODE BEGIN TIM5_Init 0 */

  /* USER CODE END TIM5_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM5_Init 1 */

  /* USER CODE END TIM5_Init 1 */
  htim5.Instance = TIM5;
  htim5.Init.Prescaler = 8400;
  htim5.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim5.Init.Period = 20;
  htim5.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim5.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim5) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim5, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim5, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM5_Init 2 */

  /* USER CODE END TIM5_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Stream0_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream0_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, VL53L0Xa_XSHUT_Pin|VL53L0Xb_XSHUT_Pin|VL53L0Xc_XSHUT_Pin|VL53L0Xd_XSHUT_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, LED_B_Pin|LED_R_Pin|LD2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(DRV_nSLEEP_GPIO_Port, DRV_nSLEEP_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : VL53L0Xa_XSHUT_Pin VL53L0Xb_XSHUT_Pin VL53L0Xc_XSHUT_Pin VL53L0Xd_XSHUT_Pin 
                           DRV_nSLEEP_Pin */
  GPIO_InitStruct.Pin = VL53L0Xa_XSHUT_Pin|VL53L0Xb_XSHUT_Pin|VL53L0Xc_XSHUT_Pin|VL53L0Xd_XSHUT_Pin 
                          |DRV_nSLEEP_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : VL53L0Xa_GPIO1_Pin VL53L0Xb_GPIO1_Pin VL53L0Xc_GPIO1_Pin VL53L0Xd_GPIO1_Pin */
  GPIO_InitStruct.Pin = VL53L0Xa_GPIO1_Pin|VL53L0Xb_GPIO1_Pin|VL53L0Xc_GPIO1_Pin|VL53L0Xd_GPIO1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : LED_B_Pin LED_R_Pin LD2_Pin */
  GPIO_InitStruct.Pin = LED_B_Pin|LED_R_Pin|LD2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : LED_G_Pin */
  GPIO_InitStruct.Pin = LED_G_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LED_G_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : DRV_nFAULT_Pin */
  GPIO_InitStruct.Pin = DRV_nFAULT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(DRV_nFAULT_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI1_IRQn);

  HAL_NVIC_SetPriority(EXTI2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI2_IRQn);

  HAL_NVIC_SetPriority(EXTI4_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI4_IRQn);

  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
