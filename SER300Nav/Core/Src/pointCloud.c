/*
 * pointCloud.c
 *
 *  Created on: May 25, 2020
 *      Author: shaun
 */

#include "pointCloud.h"
#include "string.h"
#include "math.h"
#include "stdlib.h"

#define ANGLE_TO_IDX(x) (x * NUM_POINTS / 360)

void pointCloud_clear(pointCloud_t* self) {
	memset(self->points, 0x00, sizeof(self->points));
	memset(self->edges, 0x00, sizeof(self->edges));
}

void pointCloud_setPoint(pointCloud_t* self, uint16_t angle, uint16_t value) {
	uint16_t idx;

	idx = ANGLE_TO_IDX(angle);
	self->points[idx] = value;
}

uint16_t pointCloud_getPoint(pointCloud_t* self, uint16_t angle) {
	return self->points[ANGLE_TO_IDX(angle)];
}

uint16_t pointCloud_getEdges(pointCloud_t* self, int32_t* pEdgeArray, uint16_t threshold) {
	uint16_t edgeCount = 0;
	for (uint16_t i=0; i < NUM_POINTS; i++) {
		uint16_t leftNeighbour;
		if (i == 0) {
			leftNeighbour = self->points[NUM_POINTS - 1];
		} else {
			leftNeighbour = self->points[i - 1];
		}

		// Discount false readings
		if ((leftNeighbour == 0)
				|| (self->points[i] == 0)
				|| (leftNeighbour > 3000)
				|| (self->points[i] > 3000))
			continue;

		if ((self->points[i] - leftNeighbour) > threshold) {
			*pEdgeArray = i;
			pEdgeArray++;
			edgeCount++;
		} else if ((self->points[i] - leftNeighbour) < -threshold) {
			*pEdgeArray = -i;
			pEdgeArray++;
			edgeCount++;
		}
	}
	return edgeCount;
}

uint16_t pointCloud_populateEdges(pointCloud_t* self, uint16_t threshold) {
	self->numEdges = pointCloud_getEdges(self, self->edges, threshold);
	return self->numEdges;
}

uint8_t pointCloud_getDegreesForObjectOfWidth(uint16_t distance, uint16_t width) {
	uint8_t degrees;
	degrees = (uint8_t)fabs(
				asin(
					((double)width / 2) / (double)distance)
				) * 180 / M_PI;
	return degrees;
}

//uint16_t pointCloud_getNextPositiveEdge(pointCloud_t* self, int32_t* pEdges, uint16_t numEdges) {
//	uint16_t idx = 0;
//	while(*pEdges <= 0) {
//		pEdges++;
//		idx++;
//		if (idx > numEdges) {
//			return 0xFFFF;
//		}
//	}
//	return idx;
//}
//
//uint16_t pointCloud_getNextNegativeEdge(pointCloud_t* self, int32_t* pEdges, uint16_t numEdges) {
//	uint16_t idx = 0;
//	while(*pEdges >= 0) {
//		pEdges++;
//		idx++;
//		if (idx > numEdges) {
//			return 0xFFFF;
//		}
//	}
//	return idx;
//}

uint16_t pointCloud_findNextObstacleOfWidth(pointCloud_t* self, uint16_t width) {
	for (uint16_t i = 0; i < self->numEdges; i++) {
		if (self->edges[i] < 0) {  // Negative edge. Far to near.
			// Get distance of point at edge.
			uint16_t pointIndex = abs(self->edges[i]);
			uint16_t distToPoint = self->points[pointIndex];
			uint16_t objectAngle = pointCloud_getDegreesForObjectOfWidth(distToPoint, width);
			uint16_t nextEdge = self->edges[i + 1];
			if (nextEdge > 0) {  // Positive edge. Near to far.
				// Check angle between edges
				uint16_t angleBetween = nextEdge - self->edges[i];
				if (	(angleBetween < (objectAngle*1.1))
						&& (angleBetween > (objectAngle*0.9))
				) {
					return self->edges[1];
				}
			}
		}
	}
	return 0xFFFF;
}
