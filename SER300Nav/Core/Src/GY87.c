/*
 * GY87.c
 *
 *  Created on: Apr 9, 2020
 *      Author: shaun
 *
 *  Example. Using DMA with MPU6050:
 *      https://letanphuc.net/2014/06/stm32-mpu6050-dma-i2c/
 */

#include "GY87.h"

#define RESTRICT_PITCH

void GY87_construct(GY87_t* self,
		I2C_HandleTypeDef *phi2c) {
	MPU6050_construct(&self->imu, phi2c);
	HMC5883L_construct(&self->mag, phi2c);
	kalman_construct(&self->kalmanX);
	kalman_construct(&self->kalmanY);
	kalman_construct(&self->kalmanZ);
}

void GY87_init(GY87_t* self) {
	//
	MPU6050_init(&self->imu);	// initialise accel, gyro
	HMC5883L_init(&self->mag); 	// initialise and calibrate magnetometer
	HAL_Delay(100); // wait for sensors to stabilise

	/* Set kalman and starting angle */
	MPU6050_read(&self->imu);
	HMC5883L_read(&self->mag);
	GY87_updatePitchRoll(self);
	GY87_updateYaw(self);

	kalman_setAngle(&self->kalmanX, self->roll);
	kalman_setAngle(&self->kalmanY, self->pitch);
	kalman_setAngle(&self->kalmanZ, self->yaw);

	self->lastUpdateTime = HAL_GetTick();
}

void GY87_update(GY87_t* self) {
	if (MPU6050_commCheck(&self->imu)
			|| HMC5883L_commCheck(&self->mag)) {

		double dt;
		dt = (double)(HAL_GetTick() - self->lastUpdateTime) / 1000; // Calculate delta time
		self->lastUpdateTime = HAL_GetTick();

		GY87_estimateRollPitch(self, dt);
		GY87_estimateYaw(self, dt);
	}
}

void GY87_estimateRollPitch(GY87_t* self, double dt) {
	GY87_updatePitchRoll(self);
	double gyroXrate = self->imu.gyroX / 131.0; // Convert to deg/s
	double gyroYrate = self->imu.gyroY / 131.0; // Convert to deg/s

#ifdef RESTRICT_PITCH
	if ((self->roll < -90 && self->kalAngleX > 90) || (self->roll > 90 && self->kalAngleX < -90)) {
		kalman_setAngle(&self->kalmanX, self->roll);
	} else {
		self->kalAngleX = kalman_getAngle(&self->kalmanX, self->roll, gyroXrate, dt);
	}
	if (fabs(self->kalAngleX) > 90) {
		gyroYrate = -gyroYrate;
	}
	self->kalAngleY = kalman_getAngle(&self->kalmanY, self->pitch, gyroYrate, dt);
#else
// implement by switching pitch and roll
#endif
}

void GY87_estimateYaw(GY87_t* self, double dt) {
	GY87_updateYaw(self);
	double gyroZrate = self->imu.gyroZ / 131.0; // Convert to deg/s

	// This fixes the transition problem when the yaw angle jumps between -180 and 180 degrees
	if ((self->yaw < -90 && self->kalAngleZ > 90) || (self->yaw > 90 && self->kalAngleZ < -90)) {
		kalman_setAngle(&self->kalmanZ, self->yaw);
		self->kalAngleZ = self->yaw;
	} else {
		self->kalAngleZ = kalman_getAngle(&self->kalmanZ, self->yaw, gyroZrate, dt); // calculate the angle using kalman filter
	}
}

void GY87_updatePitchRoll(GY87_t* self) {
#ifdef RESTRICT_PITCH
	double accX, accY, accZ;
	accX = self->imu.accX;
	accY = self->imu.accY;
	accZ = self->imu.accZ;

	self->roll = atan2(accY, accZ) * RAD_TO_DEG;
	self->pitch = atan(-accY / sqrt(accX * accX + accZ * accZ)) * RAD_TO_DEG;
#else
	self->roll = atan(accY / sqrt(accX * accX + accZ * accZ)) * RAD_TO_DEG;
	self->pitch = atan2(-accX, accZ) * RAD_TO_DEG;
#endif
}

void GY87_updateYaw(GY87_t* self) {
	double magX, magY, magZ;
	magX = -(double)self->mag.x;
	magY = (double)self->mag.y;
	magZ = -(double)self->mag.z;

	magX *= self->mag.gains[0];
	magY *= self->mag.gains[1];
	magZ *= self->mag.gains[2];

	magX -= self->mag.offsets[0];
	magY -= self->mag.offsets[1];
	magZ -= self->mag.offsets[2];

	double rollAngle = self->kalAngleX * DEG_TO_RAD;
	double pitchAngle = self->kalAngleY * DEG_TO_RAD;

	double Bfy = magZ * sin(rollAngle) - magY * cos(rollAngle);
	double Bfx = magX * cos(pitchAngle)
					+ magY * sin(pitchAngle)
					+ magZ * sin(pitchAngle) * cos(rollAngle);

	self->yaw = atan2(-Bfy, Bfx) * RAD_TO_DEG;
	if (self->yaw > 180 || self->yaw < -180) {
		GY87_error();
	}
	self->yaw *= -1;
}

void GY87_error() {
	uint8_t status = 0;
	status += 1;
}
